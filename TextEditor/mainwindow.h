#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include<QMainWindow>
#include<QtWidgets>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:


    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setTextChanged(bool change);
    bool getTextChanged();
    void setsaveOrNot(bool choice);
    short getCount();
    void setDeleted(bool del);
    bool getDeleted();
    static QList<MainWindow *> container;
    static QFont lastFont;
    static QSettings *setting;
    void setFont(QString x);\

    static void setCurrentFont(MainWindow *x);






private slots:
    void on_action_8_triggered();

    void on_action_Qt_triggered();

    void on_action_9_triggered();

    void on_action_10_triggered();

    void on_action_11_triggered();

    void on_action_12_triggered();

    void on_action_13_triggered();

    void on_action_3_triggered();

    void on_action_7_triggered();

    void on_plainTextEdit_textChanged();

    void on_action_15_triggered();

    void saveFile();

    void saveChanges();

    void openNewFile();

    void deleteAll(short countDelete);

    void on_action_14_triggered();

private:
    Ui::MainWindow *ui;

protected:
    bool changed;
    static short count;
    bool deleted;
    bool fontChange;
    void closeEvent(QCloseEvent *e);
    bool isSaved;
    QString saveFileName;
    bool first;
    bool stop;
    bool nondelete; // проверка на нажатие кнопки cancel при попытке сохранении нового документа



    QString openFileName;










};




#endif // MAINWINDOW_H
