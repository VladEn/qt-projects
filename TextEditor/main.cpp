#include "mainwindow.h"

#include <QApplication>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;
    //MainWindow::setCurrentFont(&w);
    MainWindow::setting = new QSettings("font.conf", QSettings::IniFormat);
    MainWindow::setting->beginGroup( "nameOfFont" );
    //if(MainWindow::setting->value( "font", -1 ).toString()!="")
    //{
        w.setFont(MainWindow::setting->value( "font", -1 ).toString());
    //}
    MainWindow::setting->endGroup();
    MainWindow::container<<&w;
    w.setWindowTitle("Новый документ"+QString::number(w.getCount()));
    w.show();


    return a.exec();
}
