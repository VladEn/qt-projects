#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QCloseEvent>
#include <QWidget>
#include <QList>





MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->action_9->setEnabled(false);
    ui->action_10->setEnabled(false);
    ui->action_12->setEnabled(false);
    ui->action_13->setEnabled(false);
    count++;
    setDeleted(false);
    setTextChanged(false);
    QObject::connect(ui->action_4,SIGNAL(triggered()),this,SLOT(openNewFile()));
    QObject::connect(ui->action_5,SIGNAL(triggered()),this,SLOT(saveChanges()));
    QObject::connect(ui->action_6,SIGNAL(triggered()),this,SLOT(saveFile()));
    setWindowIcon(QIcon(":/images/icons/windowIcon.png"));
    nondelete=true;

}

MainWindow::~MainWindow()
{
    delete ui;
}

short MainWindow::count=-1;
QList<MainWindow *> MainWindow::container{};
QFont MainWindow::lastFont;
QSettings* MainWindow::setting;


void MainWindow::setTextChanged(bool change)
{
    this->changed=change;
}

bool MainWindow::getTextChanged()
{
    return this->changed;

}

void MainWindow::setDeleted(bool del)
{
    this->deleted=del;
}

bool MainWindow::getDeleted()
{
    return this->deleted;
}

void MainWindow::saveChanges()
{

    if (openFileName!=""&&saveFileName=="")
    {        
        QFile myFile(openFileName);
        myFile.open(QIODevice::WriteOnly);
        QTextStream textflow(&myFile);
        textflow <<  ui->plainTextEdit->toPlainText();
        myFile.close();
        setWindowTitle(openFileName);
        isSaved=true;
        setTextChanged(false);
    }
    if (openFileName==""&&saveFileName!="")
    {
        QFile myFile(saveFileName);
        myFile.open(QIODevice::WriteOnly);
        QTextStream textflow(&myFile);
        textflow <<  ui->plainTextEdit->toPlainText();
        myFile.close();
        setWindowTitle(saveFileName);
        isSaved=true;
        setTextChanged(false);
    }
    if (openFileName!=""&&saveFileName!=""&&first)
    {
        QFile myFile(saveFileName);
        myFile.open(QIODevice::WriteOnly);
        QTextStream textflow(&myFile);
        textflow <<  ui->plainTextEdit->toPlainText();
        myFile.close();
        setWindowTitle(saveFileName);
        isSaved=true;
        setTextChanged(false);
    }
    if (openFileName!=""&&saveFileName!=""&&!first)
    {
        QFile myFile(openFileName);
        myFile.open(QIODevice::WriteOnly);
        QTextStream textflow(&myFile);
        textflow <<  ui->plainTextEdit->toPlainText();
        myFile.close();
        setWindowTitle(openFileName);
        isSaved=true;
        setTextChanged(false);
    }

    if (saveFileName==""&&openFileName=="")
    {
        saveFile();
    }


}

void MainWindow::saveFile()
{
    QString nameOfFile = QFileDialog::getSaveFileName(this,"","",tr("txt files(*.txt"));
    if (nameOfFile=="")
    {
        saveFileName="";
        nondelete=false;
        isSaved=false;
        setDeleted(false);
        setTextChanged(true);
    }
    else
    {
        saveFileName=nameOfFile;
            if (nameOfFile != "")
            {
                QFile myFile(nameOfFile);
                myFile.open(QIODevice::WriteOnly);
                QTextStream flow(&myFile);
                flow << ui->plainTextEdit->toPlainText();
                myFile.close();
                setWindowTitle(nameOfFile);
                isSaved=true;
                setTextChanged(false);
                first=true;
            }
    }
}

void MainWindow::deleteAll(short countDelete)
{
    if(MainWindow::container[countDelete]->getTextChanged()&&!isSaved&&!MainWindow::container[countDelete]->getDeleted())
    {

        QMessageBox::StandardButton answer = QMessageBox::question(this,"Текстовый редактор","Документ "+MainWindow::container[countDelete]->windowTitle()+" изменен. Сохранить?",QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,QMessageBox::No);

        if (answer==QMessageBox::Yes)
        {
            saveChanges();
            MainWindow::container[countDelete]->setDeleted(true);
            if(MainWindow::container[countDelete]->nondelete)
            {
               MainWindow::container[countDelete]->close();
            }
            else
            {
                MainWindow::container[countDelete]->setDeleted(false);

            }
        }
        if (answer==QMessageBox::No)
        {

            MainWindow::container[countDelete]->setDeleted(true);
            MainWindow::container[countDelete]->close();
        }
        if (answer==QMessageBox::Cancel)
        {

        }
    }
    else
    {
        MainWindow::container[countDelete]->setDeleted(true);
        MainWindow::container[countDelete]->close();
    }
}

void MainWindow::on_action_8_triggered()
{
    for (short i=0;i<MainWindow::count+1;i++)
    {
        //if(!MainWindow::container[i]->getTextChanged())
        deleteAll(i);
    }
    stop=false;
}

void MainWindow::on_action_Qt_triggered()
{
    QMessageBox::aboutQt(this,"Немного о Qt");
}

void MainWindow::on_action_9_triggered()
{

    ui->plainTextEdit->copy();
}

void MainWindow::on_action_10_triggered()
{
    ui->plainTextEdit->cut();
}

void MainWindow::on_action_11_triggered()
{
    ui->plainTextEdit->paste();
}

void MainWindow::on_action_12_triggered()
{
    ui->plainTextEdit->undo();
}

void MainWindow::on_action_13_triggered()
{
    ui->plainTextEdit->redo();
}

void MainWindow::on_action_3_triggered()
{
    MainWindow *NewWindow;
    NewWindow = new MainWindow(nullptr);    
    MainWindow::container<<NewWindow;
    NewWindow->setWindowTitle("Новый документ"+QString::number(count));
    NewWindow->ui->plainTextEdit->setFont(lastFont);
    NewWindow->show();

}

short MainWindow::getCount()
{
    return count;
}

void MainWindow::on_action_7_triggered()
{

    if(getTextChanged()&&!isSaved)
    {
        QMessageBox::StandardButton answer = QMessageBox::question(this,"Заголовок","Документ изменен. Сохранить?",QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,QMessageBox::No);
        if (answer==QMessageBox::Yes)
        {
            saveChanges();
            this->setDeleted(true);
            if(nondelete)
            {
                this->close();
            }
            else
            {
                this->setDeleted(false);

            }

        }
        if (answer==QMessageBox::No)
        {
            this->setDeleted(true);
            this->close();
        }
        if (answer==QMessageBox::Cancel)
        {

        }
    }
    else
    {
        this->setDeleted(true);
        this->close();
    }

}


void MainWindow::openNewFile()
{
    QString nameOfFile = QFileDialog::getOpenFileName(this,"", "",tr("txt files (*.txt)"));
     this->openFileName=nameOfFile;
        if (nameOfFile != "")
        {
            QFile myFile(nameOfFile);
            myFile.open(QIODevice::ReadOnly);
            QTextStream flow(&myFile);
            ui->plainTextEdit->setPlainText(flow.readAll());
            myFile.close();
            setWindowTitle(nameOfFile);
            setTextChanged(false);
            first=false;

        }
}

void MainWindow::on_plainTextEdit_textChanged()
{
    ui->action_9->setEnabled(true);
    ui->action_10->setEnabled(true);
    ui->action_12->setEnabled(true);
    ui->action_13->setEnabled(true);

    if(!getTextChanged())
    {

        this->setWindowTitle("*"+this->windowTitle());
    }
    setTextChanged(true);
    isSaved=false;

}

void MainWindow::on_action_15_triggered()
{
    QMessageBox::information(this,"О программе.","Текстовый редактор. Версия 1.000. <br>Автор: Еникеев Владислав Андреевич. <br> Группа Б17-501.</br>");
}

void MainWindow::setFont(QString x)
{
    ui->plainTextEdit->setFont(x);
}

void MainWindow::on_action_14_triggered()
{

    QFont myFont = QFontDialog::getFont(&fontChange);
    //MainWindow::lastFont=myFont;
    ui->plainTextEdit->setFont(myFont);
    if(fontChange)
        MainWindow::lastFont=myFont;
    if(!fontChange)
    {
        ui->plainTextEdit->setFont(lastFont);
    }
    setting->beginGroup( "nameOfFont" );
    setting->setValue( "font", lastFont);
    setting->endGroup();

}

void setCurrentFont(MainWindow *x)
{
    MainWindow::setting = new QSettings("font.conf", QSettings::IniFormat);
    MainWindow::setting->beginGroup( "nameOfFont" );
    if(MainWindow::setting->value( "font", -1 ).toString()!="")
    {
        x->setFont(MainWindow::setting->value( "font", -1 ).toString());
    }
    MainWindow::setting->endGroup();
}


void MainWindow::closeEvent(QCloseEvent *ptr)
{
    if(getTextChanged()&&!getDeleted()&&!isSaved)
    {
        QMessageBox::StandardButton answer = QMessageBox::question(this,"Заголовок","Документ изменен. Сохранить?",QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,QMessageBox::No);
        if (answer==QMessageBox::Yes)
        {
            saveChanges();
            this->setDeleted(true);
            if(nondelete)
            {
                this->close();
            }
            else
            {
                this->setDeleted(false);
                ptr->ignore();
            }
        }
        if (answer==QMessageBox::No)
        {
            this->setDeleted(true);
            this->close();
        }
        if (answer==QMessageBox::Cancel)
        {
            ptr->ignore();
        }
    }
    else
    {
        this->setDeleted(true);
        this->close();
    }
}

