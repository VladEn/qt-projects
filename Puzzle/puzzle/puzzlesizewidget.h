#ifndef PUZZLESIZEWIDGET_H
#define PUZZLESIZEWIDGET_H

#include <QObject>
#include <QFrame>
#include <QSize>
#include <QPainter>
#include <QApplication>
#include <QMouseEvent>
namespace Ui {
class PuzzleSizeWidget;
}

class PuzzleSizeWidget : public QFrame
{
    Q_OBJECT

    Q_PROPERTY(QSize value READ value WRITE setValue NOTIFY valueChanged)




    QSize m_value;
    QSize m_minimum;
    QSize m_maximum;
    QPixmap m_preview;
    Ui::PuzzleSizeWidget *ui;
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    QSize cellSize() const;
    void renderValue(QPainter *painter);
    void renderGrid(QPainter *painter);
    void paintEvent(QPaintEvent *event);


public:
    PuzzleSizeWidget(QWidget *p=0);
    QSize sizeHint() const;
    QSize cellAt(QPoint p);
    static int k;


    QPixmap pixmap() const;


signals:

    void valueChanged(QSize value);
    void horizontalValueChanged( int );
    void verticalValueChanged( int );
    void pixmapChanged(QPixmap pixmap);


public slots:
    void setValue( const QSize &v);
    void setMinimum(QSize min);
    void setMaximum(QSize max);
    void setPixmap(QPixmap pm);
    QSize value() const;
    QSize minimum() const;
    QSize maximum() const;
    QPixmap preview() const;


    void setPreview(const QPixmap &arg)
    {
        m_preview = arg;
    }





};

#endif // PUZZLESIZEWIDGET_H
