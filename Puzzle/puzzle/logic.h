#ifndef LOGIC_H
#define LOGIC_H
#include<QSize>
#include<QPixmap>
#include <QGraphicsScene>
#include<puzzlepiece.h>
#include "puzzlesizewidget.h"

class logic : public QGraphicsScene
{
public:
    logic();
    void setup(QSize size, QPixmap pict);
    PuzzlePiece::ConnectorPosition reverse(PuzzlePiece::ConnectorPosition pos);
};


#endif // LOGIC_H
