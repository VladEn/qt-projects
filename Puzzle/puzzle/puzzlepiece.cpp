#include "puzzlepiece.h"
#include<QGraphicsScene>
#include<QGraphicsItem>
#include<QVector>
#include<QMessageBox>

PuzzlePiece::PuzzlePiece(ConnectorPosition north, ConnectorPosition east, ConnectorPosition south, ConnectorPosition west)
{


        connector_position[North] = north;
        connector_position[South] = south;
        connector_position[West] = west;
        connector_position[East] = east;


        m_neighbours[0] = nullptr;
        m_neighbours[1] = nullptr;
        m_neighbours[2] = nullptr;
        m_neighbours[3] = nullptr;
        QPainterPath a;
        constructShape(a);
        setPath(a);


}


void PuzzlePiece::checkNeighbours(QVector<QPoint> &checked)
{
    if(checked.contains(Coordinates()))
        return;
    checked.insert(checked.begin(),Coordinates());
    find_neighbour(West);
    find_neighbour(East);
    find_neighbour(North);
    find_neighbour(South);


    if (m_neighbours[0])
        m_neighbours[0]->checkNeighbours(checked);
    if (m_neighbours[1])
        m_neighbours[1]->checkNeighbours(checked);
    if (m_neighbours[2])
        m_neighbours[2]->checkNeighbours(checked);
    if (m_neighbours[3])
        m_neighbours[3]->checkNeighbours(checked);
}


void PuzzlePiece::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QVector<QPoint> check;
    checkNeighbours(check);
    if(check.count()==all)
    {
        QMessageBox::information(nullptr,"ВЫ ВЫИГРАЛИ","УРААА!!!");
    }

}

void PuzzlePiece::setPixmap(QPixmap new_piec)
{
    piece=new_piec;
    update();
}

void PuzzlePiece::setCoordinates(QPoint point)
{
    coordinates.setX(point.x());
    coordinates.setY(point.y());
}

QVariant PuzzlePiece::itemChange(QGraphicsItem::GraphicsItemChange temp, const QVariant &value)
{
    if (temp==ItemPositionHasChanged)
       {
           QPoint newpoint=value.toPoint();
           if (m_neighbours[0])
               m_neighbours[0]->setPos(newpoint.x(),newpoint.y()-50);
           if (m_neighbours[1])
             m_neighbours[1]->setPos(newpoint.x(),newpoint.y()+50);
           if (m_neighbours[2])
               m_neighbours[2]->setPos(newpoint.x()-50,newpoint.y());
           if (m_neighbours[3])
               m_neighbours[3]->setPos(newpoint.x()+50,newpoint.y());
        }
    return QGraphicsItem::itemChange(temp, value);
}

void PuzzlePiece::setNumber(QSize picture)
{
    all=picture.width()*picture.height();
}


void PuzzlePiece::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setClipPath(path());
    QRect rect=this->boundingRect().toRect();
    painter->drawPixmap(rect.x(),rect.y(),piece);
    painter->setPen(Qt::black);
    painter->drawPath(path());

}



void PuzzlePiece::link(PuzzlePiece *pointer, PuzzlePiece::Direction direction)
{

    switch(direction)
    {
        case South:
        pointer->m_neighbours[0]=this;
        this->m_neighbours[1]=pointer;
        break;

        case North:
        pointer->m_neighbours[1]=this;
        this->m_neighbours[0]=pointer;
        break;

        case East:
        pointer->m_neighbours[2]=this;
        this->m_neighbours[3]=pointer;
        break;

        case West:
        pointer->m_neighbours[3]=this;
        this->m_neighbours[2]=pointer;
        break;
    }
}








QPoint PuzzlePiece::Coordinates()
{
    return coordinates;
}

QPixmap PuzzlePiece::getPiece()
{
    return piece;
}




void PuzzlePiece::find_neighbour(PuzzlePiece::Direction direct)
{
    PuzzlePiece* temp;
    if (direct==North && m_neighbours[0]) return;
    if (direct==South && m_neighbours[1]) return;
    if (direct==East && m_neighbours[2]) return;
    if (direct==West && m_neighbours[3]) return;

    switch(direct)
    {
    case North:
        {
            temp=(PuzzlePiece*)(scene()->itemAt(pos().x(),pos().y()-50,QTransform()));
            if(temp&&((temp->coordinates.x()==coordinates.x())&&(temp->coordinates.y()==coordinates.y()-1)))
            {
                link(temp,direct);
                temp->setPos(pos().x(),pos().y()-50);
            }
            break;
        }
    case South:
        {
           temp=(PuzzlePiece*)(scene()->itemAt(pos().x(),pos().y()+50,QTransform()));
            if(temp&&((temp->coordinates.x()==coordinates.x())&&(temp->coordinates.y()==coordinates.y()+1)))
            {
                link(temp,direct);
                temp->setPos(pos().x(),pos().y()+50);
            }
            break;
        }
    case East:
        {
            temp=(PuzzlePiece*)scene()->itemAt(pos().x()+50,pos().y(),QTransform());
            if(temp&&((temp->coordinates.x()==coordinates.x()+1)&&(temp->coordinates.y()==coordinates.y())))
            {
                link(temp,direct);
               temp->setPos(pos().x()+50,pos().y());
            }
            break;
        }
    case West:
        {
            temp=(PuzzlePiece*)scene()->itemAt(pos().x()-50,pos().y(),QTransform());
            if(temp&&((temp->coordinates.x()==coordinates.x()-1)&&(temp->coordinates.y()==coordinates.y())))
            {
                link(temp,direct);
                temp->setPos(pos().x()-50,pos().y());
            }
            break;
        }

    }

}

void PuzzlePiece::constructShape(QPainterPath &shape)
{

    shape.moveTo(size/2., size/2.);
    shape.lineTo(size/4.,size/2);

    switch(connector_position[South])
    {
        case Out:
        shape.cubicTo(size/4.,3*size/4., -size/4., 3*size/4.,-size/4.,size/2.);
        break;
        case In:
        shape.cubicTo(size/4.,size/4., -size/4., size/4.,-size/4.,size/2.);
        break;
        case None:
        shape.lineTo(-size/4.,size/2);
        break;

    }

    shape.lineTo(-size/2.,size/2.);
    shape.lineTo(-size/2.,size/4.);
    switch(connector_position[West])
    {
        case Out:
        shape.cubicTo(-3*size/4.,size/4., -3*size/4., -size/4.,-size/2.,-size/4.);
        break;
        case In:
        shape.cubicTo(-size/4.,size/4.,-size/4., -size/4.,-size/2.,-size/4.);
        break;
        case None:
        shape.lineTo(-size/2.,-size/4.);
        break;

    }

    shape.lineTo(-size/2.,-size/2.);
    shape.lineTo(-size/4.,-size/2.);
    switch(connector_position[North])
    {
        case Out:
        shape.cubicTo(-size/4.,-3*size/4., size/4., -3*size/4.,size/4.,-size/2.);break;
        case In:
        shape.cubicTo(-size/4.,-size/4., size/4., -size/4.,size/4.,-size/2.);break;
        case None:
        shape.lineTo(size/4.,-size/2.);break;


    }

    shape.lineTo(size/2,-size/2);
    shape.lineTo(size/2,-size/4);
    switch(connector_position[East])
    {
        case Out:
        shape.cubicTo(3*size/4.,-size/4., 3*size/4., size/4.,size/2.,size/4.);break;
        case In:
        shape.cubicTo(size/4.,-size/4., size/4., size/4.,size/2.,size/4.);break;
        case None:
        shape.lineTo(size/2.,size/4.);break;

    }


    shape.lineTo(size/2.,size/2.);
    shape.closeSubpath();

}


