#include "puzzlesizewidget.h"

PuzzleSizeWidget::PuzzleSizeWidget(QWidget *p):QFrame(p)
{
    m_value=QSize(5,6);
    m_minimum=QSize(2,2);
    m_maximum=QSize(8,8);

}

QSize PuzzleSizeWidget::sizeHint() const
{
    return QSize(300,300);
}

void PuzzleSizeWidget ::setMinimum(QSize min)
{

    m_minimum = min;
}


QSize PuzzleSizeWidget::value() const
{
    return m_value;
}

QSize PuzzleSizeWidget::minimum() const
{
    return m_minimum;
}



void PuzzleSizeWidget::setPixmap(QPixmap pm)
{
    m_preview = pm;
    update();
}


void PuzzleSizeWidget ::paintEvent(QPaintEvent *event)
{
    QPainter painter( this );
    renderValue(&painter);
    renderGrid(&painter);
}


void PuzzleSizeWidget::mousePressEvent(QMouseEvent *event)
{
    QPoint p=event->pos();
     QSize s;
     if(p.x()<=width() && p.y()<=height())
     {
         s=cellAt(p);
         setValue(s);
     }
     return;

}

void PuzzleSizeWidget::mouseMoveEvent(QMouseEvent *event)
{
    QPoint p=event->pos();
      QSize s;
      if(p.x()<=width() && p.y()<=height())
      {
          s=cellAt(p);
          setValue(s);
      }
      return;

}




 int PuzzleSizeWidget::k = 0;

QSize PuzzleSizeWidget::cellSize() const {
    int w = width();
    int h = height();
    int mw = maximum().width();
    int mh = maximum().height();
    int extent = qMin(w/mw, h/mh);
    return QSize(extent,extent).
 expandedTo(QApplication::globalStrut()).expandedTo(QSize(4,4));
 }

QSize PuzzleSizeWidget::cellAt(QPoint p)
{
    QSize Sizeofcell=cellSize();
    double x=p.x()/Sizeofcell.width()+1;
    double y=p.y()/Sizeofcell.height()+1;
    QSize piece = QSize(x,y);
    return piece;
}

void PuzzleSizeWidget::renderValue(QPainter * painter)
{
    QSize Sizeofsell = cellSize();
    QSize size(Sizeofsell.width()*value().width(),Sizeofsell.height()*value().height());
    if(m_preview.isNull())
    {
        painter->setBrush(Qt::gray);
        painter->drawRect(0,0,size.width(),size.height());
    }
    else
    {
        painter->drawPixmap(0,0,m_preview.scaled(size));
    }
}

void PuzzleSizeWidget ::setMaximum(QSize max)
{
    m_maximum = max;
}



void PuzzleSizeWidget::renderGrid(QPainter *painter)
{
    QSize Sizeofsell = cellSize();
    painter->setBrush(Qt::NoBrush);
    for(int x=0;x<maximum().width();x++)
        for(int y=0;y<maximum().height();y++)
        {
            painter->drawRect(x*Sizeofsell.width(),y*Sizeofsell.height(),Sizeofsell.width(),Sizeofsell.height());
        }

}


QPixmap PuzzleSizeWidget::pixmap() const
{
    return m_preview;
}

void PuzzleSizeWidget ::setValue( const QSize &v)
{
    if ( m_value == v)
        return ;
    QSize old = m_value ;
    if(v.height() > m_maximum.height() || v.width() > m_maximum.width() || v.height() < m_minimum.height() ||  v.width() < m_minimum.width()  )
        return;

    m_value = v;
    emit valueChanged(v);

    if (old.width() != v.width())
        emit horizontalValueChanged(v.width());
    if (old.height() != v.height())
        emit verticalValueChanged(v.height());
    update();
}


QSize PuzzleSizeWidget::maximum() const
{
    return m_maximum;
}

QPixmap PuzzleSizeWidget::preview() const
{
    return m_preview;
}









