#include "logic.h"


logic::logic()
{
}



PuzzlePiece::ConnectorPosition logic:: reverse(PuzzlePiece::ConnectorPosition pos)

{

    switch(pos)
    {

            case PuzzlePiece::None: return PuzzlePiece::None;
            case PuzzlePiece::In: return PuzzlePiece::Out;
            case PuzzlePiece::Out: return PuzzlePiece::In;

    }

            return PuzzlePiece::None;
}

void logic::setup(QSize size,QPixmap cartoon)

{
       this->clear();
       const int cellSize=50;
       QPixmap temp=cartoon.scaled(cellSize*size.width(),cellSize*size.height());

        PuzzlePiece::ConnectorPosition storedWest;

        QVector<PuzzlePiece::ConnectorPosition> prev(size.width(), PuzzlePiece::None);

        for(int row = 0; row < size.height(); ++row)
        {

            storedWest = PuzzlePiece::None;

            for(int col = 0; col < size.width(); ++col)
            {

                PuzzlePiece::ConnectorPosition curr[4]; // N, E, S, W

                curr[0] = reverse(prev[col]);

                curr[1] = qrand() % 2 ? PuzzlePiece::In : PuzzlePiece::Out;

                curr[2] = qrand() % 2 ? PuzzlePiece::In : PuzzlePiece::Out;

                curr[3] = reverse(storedWest);

                if(col==size.width()-1) curr[1] = PuzzlePiece::None;

                if(row==size.height()-1) curr[2] = PuzzlePiece::None;

                PuzzlePiece *newpiece = new PuzzlePiece(curr[0], curr[1],

                curr[2], curr[3]);

                this->addItem(newpiece);

                newpiece->setFlag(QGraphicsItem::ItemIsMovable);
                newpiece->setFlag(QGraphicsItem::ItemSendsGeometryChanges);


                newpiece->setPos(qrand()%500,qrand()%500);
                QRect qrectangle=newpiece->boundingRect().toRect();
                qrectangle.translate(0.5*cellSize+col*cellSize, 0.5*cellSize+row*cellSize);
                QPixmap mlc=temp.copy(qrectangle);
                newpiece->setPixmap(mlc);
                newpiece->setCoordinates(QPoint(col,row));
                newpiece->setNumber(size);


                storedWest = curr[1];

                prev[col] = curr[2];

            }

      }
}

