#include "configurationdialog.h"
#include "ui_configurationdialog.h"
#include <QFileDialog>
#include <QtWidgets>
#include "QFile"
#include <QPixmap>
#include <logic.h>
#include <QMessageBox>

ConfigurationDialog::ConfigurationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigurationDialog)
{
    ui->setupUi(this);
}



ConfigurationDialog::~ConfigurationDialog()
{
    delete ui;
}

QString ConfigurationDialog::imageFilePath()
{
    return nameOfFile;
}

void ConfigurationDialog::on_pushButton_clicked()
{
     nameOfFile = QFileDialog::getOpenFileName(this,"", "",tr("jpeg (*.jpg);;png(*.png);;bmp(*.bmp)"));

        if (nameOfFile != "")
        {
            ui->lineEdit->setText(nameOfFile);
            ui->frame->setPixmap(QPixmap(nameOfFile));
            update();
        }
}



void ConfigurationDialog::on_buttonBox_accepted()
{    
    if (!nameOfFile.isEmpty())
    {
        logic* gamelogic=new logic;
        QGraphicsView *graphv=new QGraphicsView;
        gamelogic->setup(ui->frame->value(),QPixmap(QString(this->imageFilePath())));
        graphv->setScene(gamelogic);
        graphv->show();
        this->close();
    }
    else
    {
        QMessageBox::warning(this,"Ошибка!","Выберите картинку!");
        return;
    }

}



QSize ConfigurationDialog::puzzleSize()
{
    return ui->frame->value();
}


