#ifndef PUZZLEPIECE_H
#define PUZZLEPIECE_H
#include <QGraphicsPathItem>
#include <QObject>
#include<QPainter>
#include<QStyleOptionGraphicsItem>
#include<QWidget>
#include<QPixmap>
#include<QPainterPath>
#include<QVariant>
#include<QGraphicsItem>






class PuzzlePiece : public QGraphicsPathItem
{
    QPixmap piece;
    QPoint coordinates;



public:
    enum Direction {North, East, South, West};
    enum ConnectorPosition {None, Out, In};

    PuzzlePiece(ConnectorPosition north,ConnectorPosition east,ConnectorPosition south,ConnectorPosition west);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    ConnectorPosition connector_position[4];
    qreal size=50;
    QPixmap getPiece();
    void constructShape(QPainterPath &shape);
    void link(PuzzlePiece * puz,Direction direct);
    int all;
    void setCoordinates(QPoint point);
    QPoint Coordinates();
    void setNumber(QSize s);
    QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value);
    void find_neighbour(PuzzlePiece::Direction d);
    void setPixmap(QPixmap pic);
    void checkNeighbours(QVector<QPoint> &checked);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
private:
    PuzzlePiece * m_neighbours[4];
};

#endif // PUZZLEPIECE_H
