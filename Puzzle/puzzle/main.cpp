#include "mainwindow.h"
#include <configurationdialog.h>
#include <QApplication>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <logic.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ConfigurationDialog x;
    x.exec();
    return a.exec();
}
