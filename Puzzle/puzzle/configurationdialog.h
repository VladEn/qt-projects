#ifndef CONFIGURATIONDIALOG_H
#define CONFIGURATIONDIALOG_H

#include <QDialog>

namespace Ui {
class ConfigurationDialog;
}

class ConfigurationDialog : public QDialog
{
    Q_OBJECT
private slots:
    void on_pushButton_clicked();

    void on_buttonBox_accepted();
private:
    Ui::ConfigurationDialog *ui;
    QString nameOfFile;
public:
    ~ConfigurationDialog();
    QSize puzzleSize();
    QString imageFilePath();
    explicit ConfigurationDialog(QWidget *parent = nullptr);



};

#endif // CONFIGURATIONDIALOG_H
