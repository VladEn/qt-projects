#ifndef PRODUCER_H
#define PRODUCER_H

#include <QObject>
#include <QThread>
#include <QSemaphore>
#include <QVector>
#include <cellofbuffer.h>
#include <QFile>
#include <QDebug>

class Producer : public QThread
{
    Q_OBJECT
public:
    Producer();
    Producer(QString firstFile,QString secondFile , QString thirdFile,int bufferSize , std::vector<cellOfBuffer>& buffer , QSemaphore *freeCells , QSemaphore* processedCells );



    void run() override;

private:
    QString firstFile = "C:/Users/credo/Desktop/1.txt";
    QString secondFile = "C:/Users/credo/Desktop/2.txt";
    QString thirdFile = "C:/Users/credo/Desktop/3.txt";
    int bufferSize;
    QSemaphore* freeCells;
    QSemaphore* processedCells;
    std::vector<cellOfBuffer>* buffer;
    QString firstString="";
    QString secondString="";
    QString thirdString="";
    int _count_first = 0;
    int _count_second = 0;
    int _count_third = 0;
    int firstStringLength;
    int secondStringLength;
    int thirdStringLength;
    int complete_length;
signals:
    void cellChanged(QChar letter , int flag , int place);

};

#endif // PRODUCER_H
