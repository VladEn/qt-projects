#ifndef CELLOFBUFFER_H
#define CELLOFBUFFER_H

#include <QChar>
#include <QString>

enum files{
    file_first = 1 ,
    file_second = 2 ,
    file_third = 3
};

class cellOfBuffer
{
public:
    cellOfBuffer();
    void setLetter(QChar letter)
    {
        this->letter = letter;
    }

    void setFlag(files flag)
    {
        this->flag = flag;
    }



    QChar getLetter(){return this->letter;}
    files getFlag(){return this->flag;}
private:
    QChar letter;
    files flag;
};

#endif // CELLOFBUFFER_H
