#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <fstream>
#include <QVector>
#include <cellofbuffer.h>
#include <consumer.h>
#include <worker.h>
#include <producer.h>
#include <vector>
#include <QBrush>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    Ui::MainWindow *ui;
    QString firstFile = "";
    QString secondFile = "";
    QString thirdFile = "";
    std::vector<cellOfBuffer> buffer;
    int bufferSize;
    bool firstFileChoosed = false;
    bool secondFileChoosed = false;
    bool thirdFileChoosed = false;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QString getName(int num)
    {
        if(num == 1 )
            return this->firstFile;
        if(num == 2 )
            return this->secondFile;
        else
            return this->thirdFile;
    }

    QPalette colorDefinition(int flag,QPalette pallete);

    Consumer* consumer;
    Producer* producer;
    Worker* worker;
//    Consumer1* consumer;
//    Producer1* producer;
//    Worker1* worker;
//    QString buffer_edit;


private slots:

    void on_firstFile_clicked();

    void on_secondFile_clicked();

    void on_thirdFile_clicked();
public slots:
    void firstTextChanged(QString firstString);

    void secondTextChanged(QString firstString);

    void thirdTextChanged(QString firstString);

    void cellChanged(QChar letter , int flag , int place);

    void on_pushButton_clicked();

    void firstFileCheck();

    void secondFileCheck();

    void thirdFileCheck();

    void canBegin();

signals:
    void file_first_choosed();
    void file_second_choosed();
    void file_third_choosed();
    void ready();
};
#endif // MAINWINDOW_H
