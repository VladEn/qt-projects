#ifndef CONSUMER_H
#define CONSUMER_H

#include <QObject>
#include <QThread>
#include <QSemaphore>
#include <cellofbuffer.h>
#include <qdebug.h>

class Consumer : public QThread
{
    Q_OBJECT
public:
    Consumer();
    Consumer(int bufferSize , std::vector<cellOfBuffer>& buffer , QSemaphore *usedCells , QSemaphore* freeCells , int complete_length)
    {
        this->bufferSize = bufferSize;
        this->buffer = &buffer;
        this->freeCells = freeCells;
        this->usedCells = usedCells;
        this->complete_length = complete_length;
    }
    void run() override;

signals:
    void firstStringChanged(QString firstString);
    void secondStringChanged(QString secondString);
    void thirdStringChanged(QString thirdString);
private:
    int bufferSize;
    QSemaphore* freeCells;
    QSemaphore* usedCells;
    std::vector<cellOfBuffer>* buffer;
    int complete_length;
    QString firstString = "";
    QString secondString = "";
    QString thirdString = "";

};

#endif // CONSUMER_H
