#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->ui->textFirstFile->setReadOnly(true);
    this->ui->textSecondFile->setReadOnly(true);
    this->ui->textThirdFile->setReadOnly(true);
    this->ui->textFirstFileOriginal->setReadOnly(true);
    this->ui->textSecondFileOriginal->setReadOnly(true);
    this->ui->textThirdFileOriginal->setReadOnly(true);
    this->ui->pushButton->setEnabled(false);
    buffer =  std::vector<cellOfBuffer>(10);
    bufferSize = buffer.size();
    buffer[0].setLetter('b');
    QPalette p1 = ui->buffer_1->palette();
    p1.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_1->setPalette(p1);
    //ui->buffer_1->setFrame(false);
    QPalette p2 = ui->buffer_2->palette();
    p2.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_2->setPalette(p2);
    QPalette p3 = ui->buffer_3->palette();
    p3.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_3->setPalette(p3);
    QPalette p4 = ui->buffer_4->palette();
    p4.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_4->setPalette(p4);
    QPalette p5 = ui->buffer_5->palette();
    p5.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_5->setPalette(p5);
    QPalette p6 = ui->buffer_6->palette();
    p6.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_6->setPalette(p6);
    QPalette p7 = ui->buffer_7->palette();
    p7.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_7->setPalette(p7);
    QPalette p8 = ui->buffer_8->palette();
    p8.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_8->setPalette(p8);
    QPalette p9 = ui->buffer_9->palette();
    p9.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_9->setPalette(p9);
    QPalette p10 = ui->buffer_10->palette();
    p10.setColor(QPalette::Base, Qt::cyan);
    ui->buffer_10->setPalette(p10);

    ui->buffer_1->setReadOnly(true);
    ui->buffer_2->setReadOnly(true);
    ui->buffer_3->setReadOnly(true);
    ui->buffer_4->setReadOnly(true);
    ui->buffer_5->setReadOnly(true);
    ui->buffer_6->setReadOnly(true);
    ui->buffer_7->setReadOnly(true);
    ui->buffer_8->setReadOnly(true);
    ui->buffer_9->setReadOnly(true);
    ui->buffer_10->setReadOnly(true);
    ui->firstFileCheck->setReadOnly(true);
    ui->secondFileCheck->setReadOnly(true);
    ui->thirdFileCheck->setReadOnly(true);
    connect(this,&MainWindow::file_first_choosed, this, &MainWindow::firstFileCheck);
    connect(this,&MainWindow::file_second_choosed, this, &MainWindow::secondFileCheck);
    connect(this,&MainWindow::file_third_choosed, this, &MainWindow::thirdFileCheck);
    connect(this,&MainWindow::ready, this, &MainWindow::canBegin);
    qDebug() << "Current thread: " << QThread::currentThread();
}

MainWindow::~MainWindow()
{
    delete ui;
}






void MainWindow::on_pushButton_clicked()
{
    QString str = "";
    QFile file1(this->getName(1));
    QFile file2(this->getName(2));
    QFile file3(this->getName(3));
    if ((file1.exists())&&(file1.open(QIODevice::ReadOnly))
      &&(file2.exists())&&(file2.open(QIODevice::ReadOnly))
      &&(file3.exists())&&(file3.open(QIODevice::ReadOnly)))
    {
        while(!file1.atEnd())
        {
            str=str+file1.readLine();
        }
        file1.close();
        while(!file2.atEnd())
        {
            str=str+file2.readLine();
        }
        file2.close();
        while(!file3.atEnd())
        {
            str=str+file3.readLine();
        }
        file3.close();
    }
    int complete_length = str.length();
    QSemaphore* freeCells = new QSemaphore(buffer.size());
    QSemaphore* usedCells = new QSemaphore();
    QSemaphore* processedCells = new QSemaphore();
    producer = new Producer(this->firstFile,this->secondFile,this->thirdFile,bufferSize,buffer,freeCells,processedCells);
    worker = new Worker(bufferSize,this->buffer,usedCells,processedCells,complete_length);
    consumer = new Consumer(bufferSize,this->buffer,usedCells,freeCells,complete_length);
    connect(consumer,&Consumer::firstStringChanged, this, &MainWindow::firstTextChanged);
    connect(consumer,&Consumer::secondStringChanged, this, &MainWindow::secondTextChanged);
    connect(consumer,&Consumer::thirdStringChanged, this, &MainWindow::thirdTextChanged);
    connect(producer,&Producer::cellChanged,this,&MainWindow::cellChanged);
    connect(worker,&Worker::cellChanged,this,&MainWindow::cellChanged);
    producer->start();
    worker->start();
    consumer->start();
//    producer->wait();
//    worker->wait();
//    consumer->wait()

}

void MainWindow::cellChanged(QChar letter , int flag , int place)
{
    switch(place)
    {
    case 0:{
        this->ui->buffer_1->setText(letter);
        QPalette palette = ui->buffer_1->palette();
        ui->buffer_1->setPalette(colorDefinition(flag,palette));
        break;
    }

    case 1:{
        this->ui->buffer_2->setText(letter);
        QPalette palette = ui->buffer_2->palette();
        ui->buffer_2->setPalette(colorDefinition(flag,palette));
        break;
    }
    case 2:{
        this->ui->buffer_3->setText(letter);
        QPalette palette = ui->buffer_3->palette();
        ui->buffer_3->setPalette(colorDefinition(flag,palette));
        break;
    }
    case 3:{
        this->ui->buffer_4->setText(letter);
        QPalette palette = ui->buffer_4->palette();
        ui->buffer_4->setPalette(colorDefinition(flag,palette));
        break;
    }
    case 4:{
        this->ui->buffer_5->setText(letter);
        QPalette palette = ui->buffer_5->palette();
        ui->buffer_5->setPalette(colorDefinition(flag,palette));
        break;
    }
    case 5:{
        this->ui->buffer_6->setText(letter);
        QPalette palette = ui->buffer_6->palette();
        ui->buffer_6->setPalette(colorDefinition(flag,palette));
        break;
    }
    case 6:{
        this->ui->buffer_7->setText(letter);
        QPalette palette = ui->buffer_7->palette();
        ui->buffer_7->setPalette(colorDefinition(flag,palette));
        break;
    }
    case 7:{
        this->ui->buffer_8->setText(letter);
        QPalette palette = ui->buffer_8->palette();
        ui->buffer_8->setPalette(colorDefinition(flag,palette));
        break;
    }
    case 8:{
        this->ui->buffer_9->setText(letter);
        QPalette palette = ui->buffer_9->palette();
        ui->buffer_9->setPalette(colorDefinition(flag,palette));
        break;
    }
    case 9:{
        this->ui->buffer_10->setText(letter);
        QPalette palette = ui->buffer_10->palette();
        ui->buffer_10->setPalette(colorDefinition(flag,palette));
        break;
    }
    }
}

QPalette MainWindow::colorDefinition(int flag , QPalette palette)
{
    if(flag == 1)
    {
        palette.setColor(QPalette::Base, Qt::red);
        return palette;
    }
    if(flag == 2)
    {
        palette.setColor(QPalette::Base, Qt::green);
        return palette;
    }
    if(flag == 3)
    {
        palette.setColor(QPalette::Base, Qt::blue);
        return palette;
    }
}

void MainWindow::firstTextChanged(QString firstString)
{
    this->ui->textFirstFile->clear();
    this->ui->textFirstFile->setPlainText(firstString);
}

void MainWindow::secondTextChanged(QString secondString)
{
    this->ui->textSecondFile->setPlainText(secondString);
}

void MainWindow::thirdTextChanged(QString thirdString)
{
    this->ui->textThirdFile->setPlainText(thirdString);
}


void MainWindow::on_firstFile_clicked()
{
    this->firstFile = QFileDialog::getOpenFileName(this,"", "",tr("txt files (*.txt)"));
    QFile file(this->firstFile);
    if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
    {
        emit file_first_choosed();
        firstFileChoosed = true;
        QString str="";
        while(!file.atEnd())
        {
            str=str+file.readLine();
        }
        ui->textFirstFileOriginal->setPlainText(str);
        file.close();
    }
    if(firstFileChoosed && secondFileChoosed && thirdFileChoosed)
        emit ready();
}

void MainWindow::on_secondFile_clicked()
{
    this->secondFile = QFileDialog::getOpenFileName(this,"", "",tr("txt files (*.txt)"));
    QFile file(this->secondFile);
    if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
    {
        emit file_second_choosed();
        secondFileChoosed = true;
        QString str="";
        while(!file.atEnd())
        {
            str=str+file.readLine();
        }
        ui->textSecondFileOriginal->setPlainText(str);
        file.close();
    }
    if(firstFileChoosed && secondFileChoosed && thirdFileChoosed)
        emit ready();
}

void MainWindow::on_thirdFile_clicked()
{

        this->thirdFile = QFileDialog::getOpenFileName(this,"", "",tr("txt files (*.txt)"));
        QFile file(this->thirdFile);
        if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
        {
            emit file_third_choosed();
            thirdFileChoosed = true;
            QString str="";
            while(!file.atEnd())
            {
                str=str+file.readLine();
            }
            ui->textThirdFileOriginal->setPlainText(str);
            file.close();
        }
        if(firstFileChoosed && secondFileChoosed && thirdFileChoosed)
            emit ready();
}

void MainWindow::canBegin()
{
    this->ui->pushButton->setEnabled(true);
}

void MainWindow::firstFileCheck()
{
    QPalette palette = ui->firstFileCheck->palette();
    ui->firstFileCheck->setPalette(colorDefinition(1,palette));
}


void MainWindow::secondFileCheck()
{
    QPalette palette = ui->secondFileCheck->palette();
    ui->secondFileCheck->setPalette(colorDefinition(2,palette));
}

void MainWindow::thirdFileCheck()
{
    QPalette palette = ui->thirdFileCheck->palette();
    ui->thirdFileCheck->setPalette(colorDefinition(3,palette));
}
