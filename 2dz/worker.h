#ifndef WORKER_H
#define WORKER_H

#include <QThread>
#include <cellofbuffer.h>
#include <QSemaphore>
#include <QDebug>
#include <QChar>


class Worker : public QThread
{
    Q_OBJECT
public:
    Worker();
    Worker(int bufferSize , std::vector<cellOfBuffer>& buffer , QSemaphore *usedCells , QSemaphore* processedCells , int complete_length)
    {
        this->bufferSize = bufferSize;
        this->buffer = &buffer;
        this->processedCells = processedCells;
        this->usedCells = usedCells;
        this->complete_length = complete_length;
    }

    void run() override;

private:
    int bufferSize;
    QSemaphore* processedCells;
    QSemaphore* usedCells;
    std::vector<cellOfBuffer>* buffer;
    int complete_length;
signals:
    void cellChanged(QChar letter , int flag , int place);
};

#endif // WORKER_H
