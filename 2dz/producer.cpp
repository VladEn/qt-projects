#include "producer.h"

Producer::Producer()
{

}

Producer::Producer(QString firstFile,QString secondFile , QString thirdFile,int bufferSize , std::vector<cellOfBuffer>& buffer , QSemaphore *freeCells , QSemaphore* processedCells )
{
    this->firstFile = firstFile;
    this->secondFile = secondFile;
    this->thirdFile = thirdFile;
    this->bufferSize = bufferSize;
    this->buffer = &buffer;
    this->freeCells = freeCells;
    this->processedCells = processedCells;
}

void Producer::run()
{
    qDebug() << "Producer thread: " << QThread::currentThread();

    QFile file_1(this->firstFile);
    QFile file_2(this->secondFile);
    QFile file_3(this->thirdFile);
    if ((file_1.exists())&&(file_1.open(QIODevice::ReadOnly))
      &&(file_2.exists())&&(file_2.open(QIODevice::ReadOnly))
      &&(file_3.exists())&&(file_3.open(QIODevice::ReadOnly)))
    {
        while(!file_1.atEnd())
        {
            firstString+=file_1.readLine();
        }
        file_1.close();
        while(!file_2.atEnd())
        {
            secondString+=file_2.readLine();
        }
        file_2.close();
        while(!file_3.atEnd())
        {
            thirdString+=file_3.readLine();
        }
        file_3.close();
    }
    firstStringLength = firstString.length();
    secondStringLength = secondString.length();
    thirdStringLength = thirdString.length();
    complete_length = secondStringLength + firstStringLength + thirdStringLength;
    int count = 0;
    while(count < complete_length)
    {
        int numberOfFile = rand() % 3 +1 ;
        switch(numberOfFile)
        {
            case files::file_first:
            if(_count_first  == firstStringLength)
                continue;
            freeCells->acquire();
            (*buffer)[count % bufferSize].setLetter(firstString[_count_first]);
            (*buffer)[count % bufferSize].setFlag(files::file_first);
            usleep(250000);
            emit cellChanged(firstString[_count_first], 1 , count % bufferSize);
            usleep(250000);
            processedCells->release();
            _count_first++;
            count++;
            break;

            case files::file_second:
            if(_count_second  == secondStringLength)
                continue;
            freeCells->acquire();
            (*buffer)[count % bufferSize].setLetter(secondString[_count_second]);
            (*buffer)[count % bufferSize].setFlag(files::file_second);
            usleep(250000);
            emit cellChanged(secondString[_count_second], 2 , count % bufferSize);
            usleep(250000);
            processedCells->release();
            _count_second++;
            count++;
            break;

            case files::file_third:
            if(_count_third  == thirdStringLength)
                continue;
            freeCells->acquire();
            (*buffer)[count % bufferSize].setLetter(thirdString[_count_third]);
            (*buffer)[count % bufferSize].setFlag(files::file_third);
            usleep(250000);
            emit cellChanged(thirdString[_count_third], 3 , count % bufferSize);
            usleep(250000);
            processedCells->release();
            _count_third++;
            count++;
            break;
        }

    }
}
