#include "mainwindow.h"
#include <cellofbuffer.h>
#include <QSemaphore>
#include <producer.h>

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
