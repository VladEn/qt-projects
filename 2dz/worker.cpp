#include "worker.h"

Worker::Worker()
{

}


void Worker::run()
{
    qDebug() << "Worker thread: " << QThread::currentThread();
    for (int i = 0; i < this->complete_length;i++) {
        processedCells->acquire();
        if((*buffer)[i % bufferSize].getLetter() == '/r' || (*buffer)[i % bufferSize].getLetter() == '/n')
        {}
        else
        {
            if((*buffer)[i % bufferSize].getLetter().isLower())
            {

                (*buffer)[i % bufferSize].setLetter((*buffer)[i % bufferSize].getLetter().toUpper());
                emit cellChanged((*buffer)[i % bufferSize].getLetter() , (*buffer)[i % bufferSize].getFlag() , i % bufferSize);
            }

            else
            {
                (*buffer)[i % bufferSize].setLetter((*buffer)[i % bufferSize].getLetter().toLower());
                emit cellChanged((*buffer)[i % bufferSize].getLetter() , (*buffer)[i % bufferSize].getFlag() , i % bufferSize);
            }

        }
        usedCells->release();
        usleep(250000);
    }
}
