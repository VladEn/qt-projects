#include "consumer.h"

Consumer::Consumer()
{

}

void Consumer::run()
{
    qDebug() << "Consumer thread: " << QThread::currentThread();
    for (int i = 0; i < this->complete_length; i++) {
        usleep(250000);
        usedCells->acquire();

        if((*buffer)[i % bufferSize].getFlag() == files::file_first)
        {
            firstString+=(*buffer)[i % bufferSize].getLetter();
            emit(firstStringChanged(this->firstString));
        }
        if((*buffer)[i % bufferSize].getFlag() == files::file_second)
        {
            secondString+=(*buffer)[i % bufferSize].getLetter();
            emit(secondStringChanged(this->secondString));
        }
        if((*buffer)[i % bufferSize].getFlag() == files::file_third)
        {
            thirdString+=(*buffer)[i % bufferSize].getLetter();
            emit(thirdStringChanged(this->thirdString));
        }
        freeCells->release();
//            qDebug()<<"Consumer : free"<<freeCells->available();
//            qDebug()<<"Consumer : used"<<usedCells->available();
        usleep(250000);
    }
}
