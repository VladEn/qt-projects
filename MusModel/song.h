#ifndef SONG_H
#define SONG_H

#include "item.h"
#include "album.h"

#include <QObject>
#include <QList>
#include <QString>
#include <QTime>
#include <QDataStream>



class Song : public Item
{
private:
    QTime duration;
    int songrate=0;
public:
    friend QDataStream &operator <<(QDataStream &stream, Song &song);
    friend QDataStream &operator >>(QDataStream &stream, Song &song);

    int getRate(){return this->rating;}
    void setRate(int rating){this->rating = rating;}
    int getSongrate(){return this->songrate;}
    void setSongrate(int songrate){this->songrate = songrate;}
    QTime getDuration(){return this->duration;}
    void setDuration(QTime duration){this->duration = duration;}

    Song(Item *);
    Song(const Song&) = default;
    ~Song();

    //Song& operator=(const Song&) = default;



    Artist *toArtist();
    Album *toAlbum();
    Song *toSong();
    Core *toCore();
};
#endif // SONG_H
