#ifndef ARTISTDIALOG_H
#define ARTISTDIALOG_H

#include <QDialog>

#include <QDialog>
#include <musicmodel.h>
#include <QDataWidgetMapper>

namespace Ui {
class ArtistDialog;
}

class ArtistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ArtistDialog(QWidget *parent = nullptr);
    void setModel(MusicModel*model);
    void setModelIndex(const QModelIndex &model);
    void accept();
    bool addArtist(MusicModel *model, const QModelIndex &parent);
    ~ArtistDialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ArtistDialog *ui;
    QDataWidgetMapper m_mapper;
    QPixmap photo;
};

#endif // ARTISTDIALOG_H
