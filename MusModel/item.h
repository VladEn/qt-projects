#ifndef STICK_H
#define STICK_H

#pragma once

#include <QObject>
#include <QList>
#include <QString>
#include <QDebug>

class Album;
class Core;
class Artist;
class Song;


class Item
{

public:
    Item();
    Item(Item *parent);
    Item(Item *parent, int rate);
    ~Item();

    void setName(QString name);
    QString getName();
    void setComment(QString comm);
    QString getComment();
    void setRating(int);
    int getRating();

    bool setParent(Item*);
    Item *getParent();
    bool insertChild(Item*, int);
    bool takeChild(int);
    bool removeChild(Item*);
    int childCount();
    int indexOf(Item*) const;
    Item *childAt(int) const;


    virtual Core* toCore()
    {
        return nullptr;
    }
    virtual Artist* toArtist()
    {
        return nullptr;
    }
    virtual Album* toAlbum()
    {
        return nullptr;
    }
    virtual Song* toSong()
    {
        return nullptr;
    }

protected:
    Item *parent = nullptr;
    QList<Item*> children;
    QString name;
    int rating = 4;
    QString comment;
};



#endif // STICK_H
