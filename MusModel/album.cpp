#include "album.h"
#include <song.h>
#include <QDebug>

Album::Album(Item *parent) : Item::Item(parent, 2)
{
    this->setRating(2);
}

Album::~Album()
{

}

Artist* Album::toArtist()
{
    return nullptr;
}
Album* Album::toAlbum()
{
    return this;
}
Song* Album::toSong()
{
    return nullptr;
}
Core* Album::toCore()
{
    return nullptr;
}


QDataStream & operator <<(QDataStream &stream, Album &album)
{
    stream << album.rating << album.name << album.year
           << album.picture << album.janr << album.comment << album.children.length();
    for(int i=0; i < album.childCount(); i++){
        Song * song = album.childAt(i)->toSong();
        if (song){
            stream << *song;
        }
    }
    return stream;
}

QDataStream & operator >>(QDataStream &stream, Album &album)
{

    int numberofsongs;
    int rating;
    QString name;
    int year;
    QPixmap photo;
    QString cover;
    QString comment;
    Song *songn;
    stream >> rating >> name >> year >> photo >> cover >> comment >> numberofsongs;
    album.setRating(rating);
    album.setName(name);
    album.setYear(year);
    album.setPicture(photo);
    album.setJanr(cover);
    album.setComment(comment);
    for (int i =0; i<numberofsongs; i++){
        songn = new Song(nullptr);
        songn->setRating(3);
        stream >> *songn;
        album.insertChild(songn, i);
    }
    return stream;
}
