/********************************************************************************
** Form generated from reading UI file 'albumdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.13.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ALBUMDIALOG_H
#define UI_ALBUMDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>

QT_BEGIN_NAMESPACE

class Ui_AlbumDialog
{
public:
    QGridLayout *gridLayout;
    QSplitter *splitter_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QSplitter *splitter_3;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QSplitter *splitter_4;
    QLabel *label_3;
    QSpinBox *spinBox;
    QLabel *label_4;
    QLineEdit *lineEdit_3;
    QSplitter *splitter_5;
    QLabel *label_5;
    QPlainTextEdit *plainTextEdit;
    QSplitter *splitter;
    QPushButton *pushButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *AlbumDialog)
    {
        if (AlbumDialog->objectName().isEmpty())
            AlbumDialog->setObjectName(QString::fromUtf8("AlbumDialog"));
        AlbumDialog->resize(400, 300);
        gridLayout = new QGridLayout(AlbumDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        splitter_2 = new QSplitter(AlbumDialog);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        label = new QLabel(splitter_2);
        label->setObjectName(QString::fromUtf8("label"));
        splitter_2->addWidget(label);
        lineEdit = new QLineEdit(splitter_2);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        splitter_2->addWidget(lineEdit);

        gridLayout->addWidget(splitter_2, 0, 0, 1, 1);

        splitter_3 = new QSplitter(AlbumDialog);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setOrientation(Qt::Horizontal);
        label_2 = new QLabel(splitter_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        splitter_3->addWidget(label_2);
        lineEdit_2 = new QLineEdit(splitter_3);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        splitter_3->addWidget(lineEdit_2);

        gridLayout->addWidget(splitter_3, 1, 0, 1, 1);

        splitter_4 = new QSplitter(AlbumDialog);
        splitter_4->setObjectName(QString::fromUtf8("splitter_4"));
        splitter_4->setOrientation(Qt::Horizontal);
        label_3 = new QLabel(splitter_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        splitter_4->addWidget(label_3);
        spinBox = new QSpinBox(splitter_4);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        splitter_4->addWidget(spinBox);
        label_4 = new QLabel(splitter_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        splitter_4->addWidget(label_4);
        lineEdit_3 = new QLineEdit(splitter_4);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        splitter_4->addWidget(lineEdit_3);

        gridLayout->addWidget(splitter_4, 2, 0, 1, 1);

        splitter_5 = new QSplitter(AlbumDialog);
        splitter_5->setObjectName(QString::fromUtf8("splitter_5"));
        splitter_5->setOrientation(Qt::Horizontal);
        label_5 = new QLabel(splitter_5);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        splitter_5->addWidget(label_5);
        plainTextEdit = new QPlainTextEdit(splitter_5);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        splitter_5->addWidget(plainTextEdit);

        gridLayout->addWidget(splitter_5, 3, 0, 1, 1);

        splitter = new QSplitter(AlbumDialog);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        pushButton = new QPushButton(splitter);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        splitter->addWidget(pushButton);
        buttonBox = new QDialogButtonBox(splitter);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        splitter->addWidget(buttonBox);

        gridLayout->addWidget(splitter, 4, 0, 1, 1);


        retranslateUi(AlbumDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), AlbumDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AlbumDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(AlbumDialog);
    } // setupUi

    void retranslateUi(QDialog *AlbumDialog)
    {
        AlbumDialog->setWindowTitle(QCoreApplication::translate("AlbumDialog", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("AlbumDialog", "\320\230\321\201\320\277\320\276\320\273\320\275\320\270\321\202\320\265\320\273\321\214", nullptr));
        label_2->setText(QCoreApplication::translate("AlbumDialog", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265", nullptr));
        label_3->setText(QCoreApplication::translate("AlbumDialog", "\320\223\320\276\320\264", nullptr));
        label_4->setText(QCoreApplication::translate("AlbumDialog", "\320\226\320\260\320\275\321\200", nullptr));
        label_5->setText(QString());
        pushButton->setText(QCoreApplication::translate("AlbumDialog", "\320\236\320\261\320\273\320\276\320\266\320\272\320\260...", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AlbumDialog: public Ui_AlbumDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ALBUMDIALOG_H
