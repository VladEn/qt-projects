#include "ratingdelegate.h"
#include <QPainter>




void RatingDelegate::paint(QPainter* painter, const QStyleOptionViewItem &style, const QModelIndex &index) const
{
    if (!index.isValid())
        return;
    QStyledItemDelegate::paint(painter, style, index);
    Item* item = static_cast<Item*>(index.internalPointer());
    painter->setBrush(Qt::white);
    painter->setPen(Qt::NoPen);
    painter->drawRect(QRect(style.rect));
    if (item->toSong()) {
        painter->setBrush(Qt::white);
        painter->setPen(Qt::NoPen);
        painter->drawRect(QRect(style.rect));
        painter->setBrush(Qt::red);
        painter->setPen(Qt::black);
        int rate = index.data().toInt();
        int left = style.rect.left();
        int top = style.rect.top() + QPoint(4, 4).y();

        for (int i = 0; i < 5; i++) {
            if (i == rate)
                painter->setBrush(Qt::white);
            painter->drawEllipse(QRect(QPoint(left+(QPoint(4, 4).x()+QSize(16,16).width())*i+QPoint(4, 4).x(), top), QSize(16, 16)));
        }
    }
}


QSize RatingDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(106, 24);
}
