#include "core.h"

QDataStream &operator <<(QDataStream &stream, Core &core)
{
    stream << core.childCount();
    for (int i = 0; i < core.children.length(); i++) {
        Artist *artist = core.childAt(i)->toArtist();
        if (artist) {
            stream << *artist;
        }
    }
    return stream;
}

QDataStream &operator >>(QDataStream &stream, Core &core)
{
    int numberofartists;
    Artist *artist;
    stream >> numberofartists;

    for (int i = 0; i < numberofartists; i++) {
        artist = new Artist(nullptr);
        artist->setRating(1);
        if (artist) {
            stream >> *artist;
        }
        core.insertChild(artist,i);
    }
    return stream;
}

Artist* Core::toArtist()
{
    return nullptr;
}
Album* Core::toAlbum()
{
    return nullptr;
}
Song* Core::toSong()
{
    return nullptr;
}
Core* Core::toCore()
{
    return this;
}

