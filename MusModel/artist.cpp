#include "artist.h"
#include <QDebug>


Artist::Artist(Item *parent) : Item::Item(parent, 1)
{
    this->setRating(1);
}


Artist::~Artist()
{

}

Artist* Artist::toArtist()
{
    return this;
}
Album* Artist::toAlbum()
{
    return nullptr;
}
Song* Artist::toSong()
{
    return nullptr;
}
Core* Artist::toCore()
{
    return nullptr;
}


QDataStream &operator <<(QDataStream &stream, Artist& artist)
{
    stream << artist.rating;
    stream << artist.name;
    stream << artist.picture;
    stream << artist.country;
    stream << artist.comment;

    stream << artist.childCount();

    for (int i = 0; i < artist.childCount(); i++) {
        Album *album = artist.childAt(i)->toAlbum();
        if (album) {
            stream << *album;
        }
    }
    return stream;
}


QDataStream &operator >>(QDataStream &stream, Artist &artist)
{

    int l;
    stream >> artist.rating;
    stream >> artist.name;
    stream >> artist.picture;
    stream >> artist.country;
    stream >> artist.comment;
    stream >> l;
    Album *album;


    for (int i = 0; i < l; i++) {
        album = new Album(nullptr);
        album->setRating(2);
        if (album) {
            stream >> *album;
        }
        artist.insertChild(album, i);
    }
    return stream;
}





