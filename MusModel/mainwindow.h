#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QModelIndex>
#include "musicmodel.h"
#include <QLineEdit>
#include <artistdialog.h>
#include <albumdialog.h>
#include <songdialog.h>


class Core;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void closeEvent(QCloseEvent *event) override;


signals:
    void saveFile();
public slots:
    void setchanged();

private slots:
    void setTitle();
    void save();
    void load();
    void on_action_newArtist_triggered();
    void on_action_edit_triggered();
    void on_action_newAlbum_triggered();
    void on_action_newSong_triggered();
    bool on_action_delete_triggered();
    void on_action_open_triggered();
    void on_action_save_triggered();
    void save_trigerred();
    void save(QString);
    void load(QString);


protected:

    bool changed = true;
    bool issaved = false;
    Ui::MainWindow *ui;
    QModelIndex selected;
    QString saveName="";
    MusicModel *model;
    Core *core;
    QString filename = "Music";

public slots:
    void item_selected(const QModelIndex&);
};

#endif // MAINWINDOW_H
