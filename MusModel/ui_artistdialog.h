/********************************************************************************
** Form generated from reading UI file 'artistdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.13.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ARTISTDIALOG_H
#define UI_ARTISTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>

QT_BEGIN_NAMESPACE

class Ui_ArtistDialog
{
public:
    QGridLayout *gridLayout;
    QSplitter *splitter;
    QLabel *label;
    QLineEdit *lineEdit;
    QSplitter *splitter_2;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QSplitter *splitter_4;
    QLabel *label_3;
    QPlainTextEdit *plainTextEdit;
    QSplitter *splitter_3;
    QPushButton *pushButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ArtistDialog)
    {
        if (ArtistDialog->objectName().isEmpty())
            ArtistDialog->setObjectName(QString::fromUtf8("ArtistDialog"));
        ArtistDialog->resize(400, 300);
        gridLayout = new QGridLayout(ArtistDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        splitter = new QSplitter(ArtistDialog);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        label = new QLabel(splitter);
        label->setObjectName(QString::fromUtf8("label"));
        splitter->addWidget(label);
        lineEdit = new QLineEdit(splitter);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        splitter->addWidget(lineEdit);

        gridLayout->addWidget(splitter, 0, 0, 1, 1);

        splitter_2 = new QSplitter(ArtistDialog);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        label_2 = new QLabel(splitter_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        splitter_2->addWidget(label_2);
        lineEdit_2 = new QLineEdit(splitter_2);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        splitter_2->addWidget(lineEdit_2);

        gridLayout->addWidget(splitter_2, 1, 0, 1, 1);

        splitter_4 = new QSplitter(ArtistDialog);
        splitter_4->setObjectName(QString::fromUtf8("splitter_4"));
        splitter_4->setOrientation(Qt::Horizontal);
        label_3 = new QLabel(splitter_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        splitter_4->addWidget(label_3);
        plainTextEdit = new QPlainTextEdit(splitter_4);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        splitter_4->addWidget(plainTextEdit);

        gridLayout->addWidget(splitter_4, 2, 0, 1, 1);

        splitter_3 = new QSplitter(ArtistDialog);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setOrientation(Qt::Horizontal);
        pushButton = new QPushButton(splitter_3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        splitter_3->addWidget(pushButton);
        buttonBox = new QDialogButtonBox(splitter_3);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        splitter_3->addWidget(buttonBox);

        gridLayout->addWidget(splitter_3, 3, 0, 1, 1);


        retranslateUi(ArtistDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ArtistDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ArtistDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ArtistDialog);
    } // setupUi

    void retranslateUi(QDialog *ArtistDialog)
    {
        ArtistDialog->setWindowTitle(QCoreApplication::translate("ArtistDialog", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("ArtistDialog", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265", nullptr));
        label_2->setText(QCoreApplication::translate("ArtistDialog", "\320\241\321\202\321\200\320\260\320\275\320\260", nullptr));
        label_3->setText(QString());
        pushButton->setText(QCoreApplication::translate("ArtistDialog", "\320\244\320\276\321\202\320\276...", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ArtistDialog: public Ui_ArtistDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ARTISTDIALOG_H
