#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTableView>
#include <QTreeView>
#include <QFileDialog>
#include <QDataStream>
#include <QMessageBox>
#include <QCloseEvent>
#include "musicmodel.h"
#include "ratingdelegate.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)

{
    ui->setupUi(this);
    ui->action_newAlbum->setEnabled(false);
    ui->action_newSong->setEnabled(false);
    ui->action_edit->setEnabled(false);
    ui->action_delete->setEnabled(false);
    this->core = new Core();
    this->issaved = true;
    this->load();
    connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(setchanged()));
}


MainWindow::~MainWindow()
{

}

void MainWindow::save()
{
    save(this->filename);
    issaved = true;
}

void MainWindow::load()
{
    load(this->filename);
}

void MainWindow::save(QString file)
{
    QFile writeFile(file);
    writeFile.open(QFile::WriteOnly);
    changed = false;
    issaved = true;
    this->setWindowTitle(file);
    QDataStream inFile(&writeFile);
    inFile << *(core);
    writeFile.flush();
    writeFile.close();
}
void MainWindow::setchanged()
{
    issaved = false;
    qDebug() << "fdgfdgsfdssdfsdfs";
    changed = true;
    setTitle();

}

void MainWindow::load(QString file)
{
    QFile readFile(file);
    readFile.open(QFile::ReadOnly);
    changed = false;
    this->setWindowTitle(file);
    this->filename = file;
    QDataStream outFile(&readFile);
    Core *ncore = new Core;
    outFile >> *ncore;
    readFile.close();
    delete ui->treeView->model();
    delete this->core;
    this->core = ncore;
    this->model = new MusicModel(core, static_cast<QObject*>(this));
    ui->treeView->setModel(model);
    ui->treeView->setItemDelegateForColumn(2, new RatingDelegate(model, this));
    connect(ui->treeView, SIGNAL(clicked(const QModelIndex&)), this, SLOT(item_selected(const QModelIndex&)));
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton exit;
    qDebug() << issaved;
    if (!this->issaved)
    {
        exit = QMessageBox::question( this,  tr("Музыкальная модель"), tr("Файл был изменен. Сохранить?"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::No );
    }
    if (exit == QMessageBox::Yes){
        if(this->saveName == "")
            this->on_action_save_triggered();
        else
            this->save();
    }

        this->save_trigerred();
    if (exit == QMessageBox::Cancel)
        event->ignore();


}

void MainWindow::on_action_newArtist_triggered()
{
    ArtistDialog *dialog = new ArtistDialog();
    dialog->addArtist(model, QModelIndex());
    changed = true;
    issaved = false;
    setTitle();
}

void MainWindow::on_action_edit_triggered()
{
    QModelIndex parentIndex = ui->treeView->selectionModel()->currentIndex();
    Item *parentItem=static_cast<Item*>(parentIndex.internalPointer());
    if(parentItem->toAlbum())
    {
            AlbumDialog *album = new AlbumDialog();
            album->setModel(model);
            album->setModelIndex(parentIndex);
            album->show();
    }
    if(parentItem->toArtist())
    {
            ArtistDialog *artist = new ArtistDialog();
            artist->setModel(model);
            artist->setModelIndex(parentIndex);
            artist->show();
    }
    if(parentItem->toSong())
    {

            SongDialog *song = new SongDialog();
            song->setModel(model);
            song->setModelIndex(parentIndex);
            song->show();
    }
    issaved = false;
    changed = true;
    setTitle();


}


void MainWindow::setTitle()
{
    if (changed) {
        this->setWindowTitle(this->filename + "*");
        changed = false;
    }
}

void MainWindow::item_selected(const QModelIndex& index)
{
    this->selected = index;
    Item *item = static_cast<Item*>(index.internalPointer());
    ui->action_edit->setEnabled(false);
    if (item)
    {
        ui->action_edit->setEnabled(true);
        ui->action_newArtist->setEnabled(true);
        ui->action_delete->setEnabled(true);
        if (item->toArtist()) {
            ui->action_newSong->setEnabled(false);
            ui->action_newAlbum->setEnabled(true);
        } else if (item->toAlbum()) {
            ui->action_newSong->setEnabled(true);
            ui->action_newArtist->setEnabled(false);
            ui->action_newAlbum->setEnabled(false);
        } else if (item->toSong()) {
            ui->action_newArtist->setEnabled(false);
            ui->action_newAlbum->setEnabled(false);
            ui->action_newSong->setEnabled(false);

        }
    }
    else {
         ui->action_newArtist->setEnabled(true);
    }
}

void MainWindow::on_action_newAlbum_triggered()
{
    QModelIndex cur_index = ui->treeView->selectionModel()->currentIndex();
    AlbumDialog *album= new AlbumDialog();
    album->addAlbum(model, cur_index);
    changed = true;
    issaved = false;

    setTitle();
}


void MainWindow::on_action_newSong_triggered()
{
    QModelIndex cur_index = ui->treeView->selectionModel()->currentIndex();
    SongDialog *song= new SongDialog();
    song->addSong(model, cur_index);
    changed = true;
    issaved = false;

    setTitle();
}

bool MainWindow::on_action_delete_triggered()
{
    this->selected = this->ui->treeView->selectionModel()->currentIndex();
    if (!selected.isValid()) {
        return false;
    }
    ui->treeView->model()->removeRow(this->selected.row(), this->selected.parent());

    this->selected = QModelIndex();
    ui->action_newArtist->setEnabled(true);
    ui->action_newAlbum->setEnabled(false);
    ui->action_newSong->setEnabled(false);
    ui->action_edit->setEnabled(false);
    ui->action_delete->setEnabled(false);
    changed = true;
    issaved = false;
    setTitle();
    return true;
}

void MainWindow::on_action_open_triggered()
{

    QString fileName;
    fileName = QFileDialog::getOpenFileName(this, tr("Открыть"));
    QFile readFile(fileName);
    readFile.open(QFile::ReadOnly);

    if (!readFile.isOpen()) {
        qDebug() << "file is not opened";
    }
    changed = false;
    this->filename = fileName;
    this->saveName = fileName;
    this->setWindowTitle(filename);
    QDataStream outFile(&readFile);
    Core *newcore = new Core;
    outFile >> *newcore;
    readFile.close();
    delete ui->treeView->model();
    delete this->core;
    this->core = newcore;
    this->model = new MusicModel(core, static_cast<QObject*>(this));
    ui->treeView->setModel(model);
    ui->treeView->setItemDelegateForColumn(2, new RatingDelegate(model, this));
    connect(ui->treeView, SIGNAL(clicked(const QModelIndex&)), this, SLOT(item_selected(const QModelIndex&)));
    connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(setchanged()));
}

void MainWindow::save_trigerred()
{
    QFile writeFile(this->saveName);
    writeFile.open(QFile::WriteOnly);
    changed = false;
    issaved = true;
    QDataStream inFile(&writeFile);
    inFile << *(core);
    writeFile.flush();
    writeFile.close();
}
void MainWindow::on_action_save_triggered()
{
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this, tr("Cохранить"));
    QFile writeFile(fileName);
    writeFile.open(QFile::WriteOnly);
    changed = false;
    issaved = true;
    this->filename = fileName;
    this->saveName = fileName;
    this->setTitle();
    QDataStream inFile(&writeFile);
    inFile << *(core);
    writeFile.flush();
    writeFile.close();
    this->setWindowTitle(filename);
}
