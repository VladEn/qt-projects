#include "albumdialog.h"
#include "ui_albumdialog.h"

AlbumDialog::AlbumDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AlbumDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Альбом");
    m_mapper.setSubmitPolicy(QDataWidgetMapper::ManualSubmit);
}

AlbumDialog::~AlbumDialog()
{
    delete ui;
}



void AlbumDialog::setModel(MusicModel *model)
{
    m_mapper.setModel(model);
}


bool AlbumDialog::addAlbum(MusicModel *model, const QModelIndex &parent)
{
    setModel(model);
    int row = model->rowCount(parent);
    if(!model->insertRow(row, parent))
        return false;
    QModelIndex index = model->index(row, 0, parent);
    setModelIndex(index);
    if(!exec()){
        model->removeRow(row, parent);
        return false;
    }
    return true;

}



void AlbumDialog::accept()
{
m_mapper.submit();
QDialog::accept();
}


void AlbumDialog::setModelIndex(const QModelIndex &index)
{
    qDebug() << "setIndex";
    QModelIndex parent = index.parent();
    Item *parent_item = static_cast<Item*>(index.parent().internalPointer());
    m_mapper.setRootIndex(parent);
    m_mapper.setCurrentModelIndex(index);
    ui->lineEdit->setText(parent_item->getName());
    ui->lineEdit->setReadOnly(true);
    m_mapper.addMapping(ui->lineEdit_2,0);
    m_mapper.addMapping(ui->label_5,1, "pixmap");
    m_mapper.addMapping(ui->lineEdit_3,2);
    m_mapper.addMapping(ui->plainTextEdit,3);
    m_mapper.addMapping(ui->spinBox,4);
    m_mapper.setCurrentIndex(index.row());
}
void AlbumDialog::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Открыть изображение"), tr("*.jpg"));
    if (!fileName.isEmpty())
    {
        photo = QPixmap(fileName);
        ui->label_5->setPixmap(photo.scaled(150,150));
    }
}
