#include "song.h"

Song::Song(Item *parent) : Item::Item(parent, 3)
{
    this->setRate(3);
    qDebug() << "new song";
}


Song::~Song()
{

}

QDataStream &operator <<(QDataStream &stream, Song& a)
{
    stream << a.getRate() << a.getSongrate() << a.getName() << a.getDuration() << a.getComment();
    return stream;
}

Artist* Song::toArtist()
{
    return nullptr;
}
Album* Song::toAlbum()
{
    return nullptr;
}
Song* Song::toSong()
{
    return this;
}
Core* Song::toCore()
{
    return nullptr;
}

QDataStream &operator >>(QDataStream &stream, Song &song)
{
    int rate;
    int songrate;
    QString name;
    QTime duration;
    QString comment;

    stream >> rate >>songrate>> name >> duration >> comment;
    song.setName(name);
    song.setDuration(duration);
    song.setRate(rate);
    song.setSongrate(songrate);
    song.setComment(comment);
    return stream;
}
