#include "songdialog.h"
#include "ui_songdialog.h"

SongDialog::SongDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SongDialog)
{
    ui->setupUi(this);
    m_mapper.setSubmitPolicy(QDataWidgetMapper::ManualSubmit);
    this->setWindowTitle(tr("Песня"));

}

SongDialog::~SongDialog()
{
    delete ui;
}



void SongDialog::setModel(MusicModel *model)
{
    qDebug() << "setModel";
    m_mapper.setModel(model);
}

void SongDialog::setModelIndex(const QModelIndex &index)
{
    qDebug() << "setIndex";
    QModelIndex parent = index.parent();
    Item *parent_item = static_cast<Item*>(index.parent().internalPointer());
    ui->lineEdit->setText(parent_item->getName());
    ui->lineEdit->setReadOnly(true);
    ui->lineEdit->setEnabled(false);
    m_mapper.setRootIndex(parent);
    m_mapper.setCurrentModelIndex(index);
    m_mapper.addMapping(ui->lineEdit_2,0);
    m_mapper.addMapping(ui->timeEdit,1);
    m_mapper.addMapping(ui->spinBox,2);
    m_mapper.addMapping(ui->plainTextEdit,3);
    m_mapper.setCurrentIndex(index.row());
}

void SongDialog::accept()
{
    m_mapper.submit();
    QDialog::accept();
}

bool SongDialog::addSong(MusicModel *model, const QModelIndex &parent)
{

    setModel(model);
    int row = model->rowCount(parent);
    if(!model->insertRow(row, parent))
        return false;
    QModelIndex index = model->index(row, 0, parent);
    setModelIndex(index);
    if(!exec()){
        model->removeRow(row, parent);
        return false;
    }
    return true;

}
