#ifndef ALBUMDIALOG_H
#define ALBUMDIALOG_H

#include <QDialog>
#include <musicmodel.h>
#include <QDataWidgetMapper>
#include <QFileDialog>
#include <QMessageBox>

namespace Ui {
class AlbumDialog;
}

class AlbumDialog : public QDialog
{
    Q_OBJECT
private:
    QPixmap photo;
public:
    explicit AlbumDialog(QWidget *parent = nullptr);
    ~AlbumDialog() override;
    void setModel(MusicModel* model);
    void setModelIndex(const QModelIndex &modelIndex);
    void accept() override;
    bool addAlbum(MusicModel *model, const QModelIndex &parent);
private slots:
    void on_pushButton_clicked();

private:
    Ui::AlbumDialog *ui;
    QDataWidgetMapper m_mapper;
};

#endif // ALBUMDIALOG_H
