#include "item.h"
#include <QDebug>

Item::Item()
{
//    qDebug() << "opa";
}



Item::Item(Item *parent)
{
    this->parent = parent;
    if (parent)
        parent->insertChild((Item*)this,-1);
}

Item::Item(Item *parent, int rating)
{
    this->parent = parent;
    this->rating = rating;
    if (parent && parent->getRating() < rating)
        parent->insertChild((Item*)this,-1);
}

Item::~Item()
{
    this->parent = nullptr;
    for(int i =0; i < this->children.count();i++)
    {
        delete children[i];
    }
    children.clear();
}

bool Item::setParent(Item* parent)
{
    if(this->parent == parent)
        return false;
    if(parent)
    {
        if(this->parent)
        {
            parent->takeChild(parent->indexOf(this));
        }
        this->parent = parent;
        this->parent->insertChild(this,parent->childCount());
    }
    else this->parent=parent;

}

Item* Item::childAt(int num) const
{
    if(num>=0 && num<this->children.length())
        return this->children[num];
    else
        return nullptr;

}

Item* Item::getParent()
{
    return this->parent;
}


bool Item::insertChild(Item* child, int position)
{
    if(!child)
        return false;
    bool error=false;
    for(int i=0;i<this->children.count();i++)
    {
        if (this->children[i]==child)
            error = true;
    }


    if(!error)
    {
       if(position <0 || position > this->children.count())
       {
           this->children.insert(children.count(),child);
       }
       else
           this->children.insert(position,child);
       if(child->parent==nullptr)
       {
          child->setParent(this);
       }
    }
    else child->setParent(this);
}

bool Item::takeChild(int position)
{
    if (position < children.length()) {
        children.at(position)->~Item();
        children.removeAt(position);
        return true;
    } else
        return false;
}


bool Item::removeChild(Item*a)
{
    if (children.contains(a)) {
        children.removeOne(a);
        a->~Item();
        return true;
    } else
        return false;
}

int Item::indexOf(Item* child) const
{
    return this->children.indexOf(child,0);
}


int Item::childCount()
{
    return this->children.length();
}


void Item::setName(QString name)
{
    this->name = name;
}
QString Item::getName()
{
    return name;
}
void Item::setComment(QString comment)
{
    this->comment = comment;
}
QString Item::getComment()
{
    return comment;
}
void Item::setRating(int rate)
{
    this->rating = rate;
}
int Item::getRating()
{
    return this->rating;
}
