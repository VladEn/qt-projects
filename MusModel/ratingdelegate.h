#ifndef RATINGDELEGATE_H
#define RATINGDELEGATE_H

#include <QStyledItemDelegate>
#include <QSize>
#include <QPoint>
#include <QColor>
#include "musicmodel.h"


class RatingDelegate : public QStyledItemDelegate
{
public:
    RatingDelegate();
    explicit RatingDelegate(MusicModel *model, QWidget *parent = nullptr) : QStyledItemDelegate(parent), model(model)
    {

    }

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void paint(QPainter*, const QStyleOptionViewItem &, const QModelIndex &) const override;

protected:
    MusicModel *model;
};

#endif // RATINGDELEGATE_H
