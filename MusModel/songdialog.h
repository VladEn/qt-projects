#ifndef SONGDIALOG_H
#define SONGDIALOG_H

#include <QDialog>
#include <musicmodel.h>
#include <QDataWidgetMapper>

namespace Ui {
class SongDialog;
}

class SongDialog : public QDialog
{
    Q_OBJECT

public:
    ~SongDialog();
    explicit SongDialog(QWidget *parent = nullptr);
    void accept();
    bool addSong(MusicModel *model, const QModelIndex &parent);
    void setModel(MusicModel*model);
    void setModelIndex(const QModelIndex &model);


private:
    Ui::SongDialog *ui;
    QDataWidgetMapper m_mapper;
};

#endif // SONGDIALOG_H
