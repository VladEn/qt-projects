/********************************************************************************
** Form generated from reading UI file 'songdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.13.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SONGDIALOG_H
#define UI_SONGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTimeEdit>

QT_BEGIN_NAMESPACE

class Ui_SongDialog
{
public:
    QGridLayout *gridLayout;
    QSplitter *splitter_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QSplitter *splitter_3;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QSplitter *splitter_5;
    QLabel *label_3;
    QTimeEdit *timeEdit;
    QSplitter *splitter_4;
    QLabel *label_4;
    QSpinBox *spinBox;
    QSplitter *splitter;
    QLabel *label_5;
    QPlainTextEdit *plainTextEdit;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SongDialog)
    {
        if (SongDialog->objectName().isEmpty())
            SongDialog->setObjectName(QString::fromUtf8("SongDialog"));
        SongDialog->resize(400, 300);
        gridLayout = new QGridLayout(SongDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        splitter_2 = new QSplitter(SongDialog);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        label = new QLabel(splitter_2);
        label->setObjectName(QString::fromUtf8("label"));
        splitter_2->addWidget(label);
        lineEdit = new QLineEdit(splitter_2);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        splitter_2->addWidget(lineEdit);

        gridLayout->addWidget(splitter_2, 0, 0, 1, 1);

        splitter_3 = new QSplitter(SongDialog);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setOrientation(Qt::Horizontal);
        label_2 = new QLabel(splitter_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        splitter_3->addWidget(label_2);
        lineEdit_2 = new QLineEdit(splitter_3);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        splitter_3->addWidget(lineEdit_2);

        gridLayout->addWidget(splitter_3, 1, 0, 1, 1);

        splitter_5 = new QSplitter(SongDialog);
        splitter_5->setObjectName(QString::fromUtf8("splitter_5"));
        splitter_5->setOrientation(Qt::Horizontal);
        label_3 = new QLabel(splitter_5);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        splitter_5->addWidget(label_3);
        timeEdit = new QTimeEdit(splitter_5);
        timeEdit->setObjectName(QString::fromUtf8("timeEdit"));
        splitter_5->addWidget(timeEdit);

        gridLayout->addWidget(splitter_5, 2, 0, 1, 1);

        splitter_4 = new QSplitter(SongDialog);
        splitter_4->setObjectName(QString::fromUtf8("splitter_4"));
        splitter_4->setOrientation(Qt::Horizontal);
        label_4 = new QLabel(splitter_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        splitter_4->addWidget(label_4);
        spinBox = new QSpinBox(splitter_4);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setMinimum(1);
        spinBox->setMaximum(5);
        splitter_4->addWidget(spinBox);

        gridLayout->addWidget(splitter_4, 3, 0, 1, 1);

        splitter = new QSplitter(SongDialog);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        label_5 = new QLabel(splitter);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        splitter->addWidget(label_5);
        plainTextEdit = new QPlainTextEdit(splitter);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        splitter->addWidget(plainTextEdit);

        gridLayout->addWidget(splitter, 4, 0, 1, 1);

        buttonBox = new QDialogButtonBox(SongDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 5, 0, 1, 1);


        retranslateUi(SongDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SongDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SongDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SongDialog);
    } // setupUi

    void retranslateUi(QDialog *SongDialog)
    {
        SongDialog->setWindowTitle(QCoreApplication::translate("SongDialog", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("SongDialog", "\320\220\320\273\321\214\320\261\320\276\320\274", nullptr));
        label_2->setText(QCoreApplication::translate("SongDialog", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265", nullptr));
        label_3->setText(QCoreApplication::translate("SongDialog", "\320\237\321\200\320\276\320\264\320\276\320\273\320\266\320\270\321\202\320\265\320\273\321\214\320\275\320\276\321\201\321\202\321\214", nullptr));
        label_4->setText(QCoreApplication::translate("SongDialog", "\320\236\321\206\320\265\320\275\320\272\320\260", nullptr));
        label_5->setText(QCoreApplication::translate("SongDialog", "\320\232\320\276\320\274\320\274\320\265\320\275\321\202\320\260\321\200\320\270\320\271", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SongDialog: public Ui_SongDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SONGDIALOG_H
