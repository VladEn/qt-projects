#ifndef ARTIST_H
#define ARTIST_H

#include "item.h"
#include "album.h"

#include <QObject>
#include <QList>
#include <QString>
#include <QDebug>
#include <QPixmap>
#include <QDataStream>




class Artist : public Item
{

protected:
    QString country;
    QPixmap picture;

public:    
    Artist *toArtist();
    Album *toAlbum();
    Song *toSong();
    Core *toCore();

    friend QDataStream &operator <<(QDataStream& stream, Artist& a);
    friend QDataStream &operator >>(QDataStream &stream, Artist &a);

    Artist(Item *);
    Artist(const Artist&) = default;
    ~Artist();
    //Artist& operator=(const Artist&) = default;

    void setCountry(QString country){this->country = country;}
    QString getCountry(){return country;}
    bool setPicture(QPixmap picture){this->picture = picture;return true;}
    QPixmap getPicture(){return picture;}
};
#endif // ARTIST_H
