#include "artistdialog.h"
#include "ui_artistdialog.h"
#include <QFileDialog>
#include <QMessageBox>

ArtistDialog::ArtistDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ArtistDialog)
{
    ui->setupUi(this);
    m_mapper.setSubmitPolicy(QDataWidgetMapper::ManualSubmit);
    this->setWindowTitle(tr("Артист"));

}

ArtistDialog::~ArtistDialog()
{
    delete ui;
}

void ArtistDialog::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Открыть изображение"), tr("*.png ;*.jpg"));
    if (!fileName.isEmpty())
    {
        this->photo=QPixmap(fileName);
        ui->label_3->setPixmap(photo.scaled(100,100));
    }
}

void ArtistDialog::accept()
{
    m_mapper.submit();
    QDialog::accept();
}

bool ArtistDialog::addArtist(MusicModel *model, const QModelIndex &parent)
{
    setModel(model);
    int row = model->rowCount(parent);
    if(!model->insertRow(row, parent))
        return false;
    QModelIndex index = model->index(row, 0, parent);
    setModelIndex(index);
    if(!exec()){
        model->removeRow(row, parent);
        return false;
    }
    return true;

}

void ArtistDialog::setModel(MusicModel *model)
{
    m_mapper.setModel(model);
    qDebug() << "setModel";
}

void ArtistDialog::setModelIndex(const QModelIndex &index)
{
    qDebug() << "setIndex";
    QModelIndex parent = index.parent();
    m_mapper.setRootIndex(parent);
    m_mapper.setCurrentModelIndex(index);
    m_mapper.addMapping(ui->lineEdit,0);
    m_mapper.addMapping(ui->label_3,1, "pixmap");
    m_mapper.addMapping(ui->lineEdit_2,2);
    m_mapper.addMapping(ui->plainTextEdit,3);
    m_mapper.setCurrentIndex(index.row());
}
