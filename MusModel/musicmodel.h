#ifndef MUSICMODEL_H
#define MUSICMODEL_H

#include "core.h"
#include <QAbstractItemModel>
#include "item.h"



class MusicModel : public QAbstractItemModel
{
    Q_OBJECT

protected:
    Core *root;

public:
    explicit MusicModel(Core *root, QObject *parent = 0);

    friend QDataStream& operator<<(QDataStream& stream, const MusicModel& model){stream << model.root;return stream;}
    friend QDataStream& operator>>(QDataStream& stream, MusicModel& model){stream >> *(model.root);return stream;}
    int rowCount(const QModelIndex &parent=QModelIndex()) const override;
    bool insertRows(int row, int count, const QModelIndex &parent=QModelIndex()) override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    QModelIndex index(int row, int column, const QModelIndex &parent=QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const override;
    QVariant headerData(int section,Qt::Orientation orientation, int role=Qt::DisplayRole) const override;
    QModelIndex parent(const QModelIndex &childIndex) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    int columnCount(const QModelIndex &parent=QModelIndex()) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    Item* getPointer(const QModelIndex &index) const;

};

#endif // MUSICMODEL_H
