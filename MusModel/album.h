#ifndef ALBUM_H
#define ALBUM_H

#include "item.h"
#include "artist.h"
#include "song.h"

#include <QObject>
#include <QList>
#include <QString>
#include <QDataStream>
#include <QImage>
#include <QPixmap>


class Album : public Item
{
private:
    QString janr;
    int year;
    QPixmap picture;
public:
    friend QDataStream &operator <<(QDataStream &stream, Album &a);
    friend QDataStream &operator >>(QDataStream &stream, Album &a);

    int getYear() const {return this->year;}
    void setYear(int year){this->year = year;}
    QString getJanr() const{return this->janr;}
    void setJanr(QString cover){this->janr = cover;}
    const QPixmap& getPicture() const{return this->picture;}
    void setPicture(const QPixmap& photo){this->picture = photo;}


    Album(Item *);
    Album(const Album&) = default;
    ~Album();
    //Album& operator=(const Album&) = default;



    Artist *toArtist();
    Album *toAlbum();
    Song *toSong();
    Core *toCore();
};
#endif // ALBUM_H
