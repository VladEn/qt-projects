#include "musicmodel.h"
#include "album.h"
#include "item.h"
#include "core.h"
#include "song.h"
#include <QVariant>
#include "artist.h"



MusicModel::MusicModel(Core *root, QObject *parent) : QAbstractItemModel(parent), root(root)
{

}



bool MusicModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Item *parentItem = this->getPointer(parent);
    Item *child = nullptr;

    beginInsertRows(parent, row, row + count - 1);
    if(parentItem == this->root) {
        child =  static_cast<Item*>(new Artist(nullptr));
        child ->setParent(parentItem);
    }
    else if(parentItem->toArtist()){
        child =  static_cast<Item*>(new Album(nullptr));
        child ->setParent(parentItem);
    }
    else if(parentItem->toAlbum()){
        child =  static_cast<Item*>(new Song(nullptr));
        child ->setParent(parentItem);
    }
    else {
        return false;
    }
    endInsertRows();
    return true;
}


QVariant MusicModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if(role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    Item *item = getPointer(index);
    if(item==root)
    {
        return QVariant();
    }
    switch(role)
    {
        case Qt::DisplayRole:
            switch(index.column())
            {
            case 0:
                return item->getName();
            case 1:
                if (item->toSong())
                    return item->toSong()->getDuration().toString("hh:mm:ss");
                break;
            case 2:
                if (item->toSong())
                    return item->toSong()->getSongrate();
                break;
            case 3:
                if (item->toSong() || item->toAlbum() || item->toArtist() )
                    return item->getComment();
                break;
            }
            break;

        case Qt::EditRole:
            switch(index.column())
            {
            case 0:
                return item->getName();
            case 1:
                if (item->toAlbum())
                    return item->toAlbum()->getPicture();
                if (item->toSong())
                    return item->toSong()->getDuration().toString("hh:mm:ss");
                if (item->toArtist())
                    return item->toArtist()->getPicture();
                break;
            case 2:
                if (item->toSong())
                    return item->toSong()->getSongrate();
                if (item->toAlbum())
                    return item->toAlbum()->getJanr();
                if (item->toArtist())
                    return item->toArtist()->getCountry();
                break;
            case 3:
            return item->getComment();
            case 4:
                if (item->toAlbum())
                    return item->toAlbum()->getYear();
                break;
            }
    }

    return QVariant();
}

QVariant MusicModel::headerData(int column,Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole || orientation != Qt::Horizontal)
        return QVariant();
    switch(column)
    {
    case 0:
        return QVariant("Название(Имя)");
    case 1:
        return QVariant("Продолжительность");
    case 2:
        return QVariant("Оценка");
    case 3:
        return QVariant("Комментарий");
    case 4:
    default:
        return QVariant();
    }
}




QModelIndex MusicModel::parent(const QModelIndex &childIndex) const
{
    Item * child = getPointer(childIndex);
    Item * parent = child->getParent();
    Item * grandparent = parent->getParent();
    if (child == root || parent == root)
        return QModelIndex();
    return createIndex(grandparent->indexOf(parent), 0, parent);
}

Qt::ItemFlags MusicModel::flags(const QModelIndex &index) const
{
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}



bool MusicModel::removeRows(int row, int count, const QModelIndex &parent)
{
    bool remove;
    beginRemoveRows(parent, row, row + count - 1);
    remove = this->getPointer(parent)->takeChild(row);
    endRemoveRows();
    return remove;
}

QModelIndex MusicModel::index(int row, int column, const QModelIndex &parent) const
{
    Item *parentItem = this->getPointer(parent);
    if(row >=0 && row< parentItem->childCount())
        if(parentItem->childAt(row))
        {
            return createIndex(row, column, parentItem->childAt(row));
        }
    return QModelIndex();
}

int MusicModel::rowCount(const QModelIndex &parent) const
{
    Item* parentItem=nullptr;
    if(parent.isValid())
        parentItem = getPointer(parent);
    if(!parent.isValid())
        return root->childCount();
    return parentItem->childCount();
}


int MusicModel::columnCount(const QModelIndex &parent) const
{
    Item * item = getPointer(parent);
    if (item->toArtist())
        return 5;
    if (item->toAlbum())
        return 4;
    if (item->toSong())
        return 0;
    if (item->toCore())
        return 4;
    return 0;
}


bool MusicModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid() || role != Qt::EditRole)
        return false;
    Item *item = getPointer(index);
    switch (index.column())
    {
    case 0:
        item->setName(value.toString());
        break;
    case 1:
        if (item->toSong())
            item->toSong()->setDuration(value.toTime());
        if (item->toAlbum())
            item->toAlbum()->setPicture(value.value<QPixmap>());
        if (item->toArtist())
            item->toArtist()->setPicture(value.value<QPixmap>());
        break;
    case 2:
        if (item->toSong())
            item->toSong()->setSongrate(value.toInt());
        if (item->toAlbum())
            item->toAlbum()->setJanr(value.toString());
        if (item->toArtist())
            item->toArtist()->setCountry(value.toString());
        break;
    case 3:
        item->setComment(value.toString());
        break;
    case 4:
        if (item->toAlbum())
            item->toAlbum()->setYear(value.toInt());
        break;
    }
    emit dataChanged(index, index);
    return true;
}

Item* MusicModel::getPointer(const QModelIndex &index) const
{
    if (index.isValid()) {
        Item *item = static_cast<Item*>(index.internalPointer());
        return item;
    }
    return this->root;
}

