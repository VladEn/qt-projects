#ifndef CORE_H
#define CORE_H

#include <QPixmap>
#include <QDataStream>
#include "item.h"
#include "artist.h"
#include <QString>
#include <QDebug>

class Core : public Item
{
public:
    friend QDataStream &operator >>(QDataStream &stream, Core &a);
    friend QDataStream &operator <<(QDataStream &stream, Core &a);

    Artist *toArtist();
    Album *toAlbum();
    Song *toSong();
    Core *toCore();

    Core(Item* parent = nullptr) : Item(parent, 0){this->setRating(0);}
};
#endif // CORE_H
