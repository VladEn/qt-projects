#include "chat.h"
#include <QDebug>
#include <QMessageBox>

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCommandLineParser parser;
    parser.addHelpOption();
    QCommandLineOption newport(QStringList() << "p");
    newport.setDefaultValue("2400");
    newport.setDescription("-p - this is port");
    parser.addOption(newport);
    QCommandLineOption newnick(QStringList() << "n");
    newnick.setDescription("-n - this is name");
    newnick.setDefaultValue("UnknownUser");
    parser.addOption(newnick);
    parser.process(a);
    QStringList arguments;
    arguments = parser.positionalArguments();
    QString nickname = "UnknownUser";
    int port = 1633;
    if(arguments.count() == 2 ){
        port = arguments[0].toInt();
        nickname = arguments[1];
    }
    chat w(nullptr,nickname,port);
    w.show();
    return a.exec();
}
