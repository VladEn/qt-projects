#include "chat.h"
#include "ui_chat.h"
#include <QMessageBox>


chat::chat(QWidget *parent ,QString name, int port)
    : QWidget(parent)
    , ui(new Ui::chat)
{
    ui->setupUi(this);
    ui->plainTextEdit->setReadOnly(true);
    this->port = port;
    this->nickname = name;
    setWindowTitle("Chat ["+name+"@"+QString::number(port)+"]");
    socket.bind(quint16(port),QAbstractSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);
    connect(&socket,SIGNAL(readyRead()),this,SLOT(readnow()));
    connect(&socket,SIGNAL(disconnect()),this,SLOT(sockDisc()));
    send("Hello",join);
}

chat::~chat()
{
    send("bye",quit);
    emit(disconnect());
    delete ui;
}

void chat::setName(QString newname)
{
    this->nickname = newname;
}

void chat::sockDisc()
{
    qDebug()<<"byebye";
    socket.deleteLater();
}


void chat::readnow()
{
    QByteArray datagr;
    while (socket.hasPendingDatagrams()) {
        datagr.resize(socket.pendingDatagramSize());
        socket.readDatagram(datagr.data(), datagr.size());
        QDataStream in(datagr);
        QString sender;
        QString message_;
        int action;
        in >> action >> sender >> message_;
        switch(action){
        case message:{
            if(sender == nickname){
                QTextCharFormat fmt = ui->plainTextEdit->currentCharFormat();
                fmt.setForeground(QBrush(Qt::red));
                ui->plainTextEdit->setCurrentCharFormat(fmt);
                QTime currentTime = QTime::currentTime();
                this->ui->plainTextEdit->appendPlainText("["+currentTime.toString("hh:mm:ss")+"]");
                this->ui->plainTextEdit->insertPlainText(sender);
                this->ui->plainTextEdit->insertPlainText(":");
                fmt = ui->plainTextEdit->currentCharFormat();
                fmt.clearForeground();
                ui->plainTextEdit->setCurrentCharFormat(fmt);
            }
            else{
                QTextCharFormat fmt = ui->plainTextEdit->currentCharFormat();
                fmt.setForeground(QBrush(Qt::blue));
                ui->plainTextEdit->setCurrentCharFormat(fmt);
                QTime currentTime = QTime::currentTime();
                this->ui->plainTextEdit->appendPlainText("["+currentTime.toString("hh:mm:ss")+"]");
                this->ui->plainTextEdit->insertPlainText(sender);
                this->ui->plainTextEdit->insertPlainText(":");
                fmt = ui->plainTextEdit->currentCharFormat();
                fmt.clearForeground();
                ui->plainTextEdit->setCurrentCharFormat(fmt);
            }
            ui->plainTextEdit->insertPlainText(message_);
            break;
        }
        case changeName:{

            QList<QListWidgetItem *> widg = ui->listWidget->findItems(message_,Qt::MatchExactly);
            widg[0]->setText(sender);
            ui->listWidget->sortItems();
            QTextCharFormat fmt = ui->plainTextEdit->currentCharFormat();
            fmt.setForeground(QBrush(Qt::gray));
            ui->plainTextEdit->setCurrentCharFormat(fmt);
            this->ui->plainTextEdit->appendPlainText(message_ + " has changed name to " + sender);
            fmt = ui->plainTextEdit->currentCharFormat();
            fmt.clearForeground();
            ui->plainTextEdit->setCurrentCharFormat(fmt);
            break;
        }

        case join:{
            if(sender != nickname){
                QList<QListWidgetItem *> widg = ui->listWidget->findItems(sender,Qt::MatchExactly);
                if(widg.isEmpty()){
                    ui->listWidget->addItem(sender);
                    ui->listWidget->sortItems();
                    send("Hello",join);
                }

            }
            else{
                QList<QListWidgetItem *> widg = ui->listWidget->findItems(sender,Qt::MatchExactly);
                if(widg.isEmpty()){
                    ui->listWidget->addItem(sender);
                    ui->listWidget->sortItems();
                }

            }
            break;
        }
        case quit:{
            QList<QListWidgetItem *> widg = ui->listWidget->findItems(sender,Qt::MatchExactly);
            QTextCharFormat fmt = ui->plainTextEdit->currentCharFormat();
            fmt.setForeground(QBrush(Qt::gray));
            ui->plainTextEdit->setCurrentCharFormat(fmt);
            this->ui->plainTextEdit->appendPlainText(sender+" has left from chat");
            fmt = ui->plainTextEdit->currentCharFormat();
            fmt.clearForeground();
            ui->plainTextEdit->setCurrentCharFormat(fmt);
            if(!widg.isEmpty()){
                delete ui->listWidget->takeItem(ui->listWidget->row(widg[0]));
                ui->listWidget->sortItems();
            }
            break;
        }

        default:
            qDebug()<<"-1";


        }

    }

}


void chat::send(QString message, int action)
{
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out << action << this->nickname << message;
    if (socket.isValid()){
        socket.writeDatagram(data, QHostAddress::Broadcast, quint16(port));
       }
}




void chat::on_pushButton_clicked()
{
    send(this->ui->lineEdit->text(),message);
    this->ui->lineEdit->clear();
}

void chat::on_pushButton_2_clicked()
{
    QString oldname = this->nickname;
    QString newname = this->ui->lineEdit_2->text();
    QList<QListWidgetItem *> widg = this->ui->listWidget->findItems(newname,Qt::MatchExactly);
    if(widg.isEmpty()){
        if(ui->lineEdit_2->text()!=""){
            this->nickname = ui->lineEdit_2->text();
            ui->lineEdit_2->clear();
            send(oldname,changeName);
        }
        setWindowTitle("Chat ["+nickname+"@"+QString::number(port)+"]");
    }
    else{
        QMessageBox::warning(this,"Новое имя","Такое имя уже существует!");
    }
}

