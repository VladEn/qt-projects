#ifndef CHAT_H
#define CHAT_H

#include <QWidget>
#include <QUdpSocket>
#include <QtNetwork>
#include <QDebug>
#include <QIODevice>

QT_BEGIN_NAMESPACE
namespace Ui { class chat; }
QT_END_NAMESPACE



class chat : public QWidget
{
    Q_OBJECT

public slots:
    void sockDisc();
public:
    enum Actions{message,join,quit,changeName};
    chat(QWidget *parent = nullptr,QString name="UnknownUser",int port = 1633 );
    ~chat();
    QUdpSocket socket;
    void setName(QString newname);
signals:
    void disconnected();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void send(QString msg, int option);
    void readnow();

private:
    Ui::chat *ui;
    QString nickname;
    int port;
};
#endif // CHAT_H
