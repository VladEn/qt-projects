#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "../myplugin/plugin.h"



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);
    ui->choosePathLineEdit->setReadOnly(true);
    ui->choosePathButton->setEnabled(false);
    ui->startButton->setEnabled(false);
    ui->stopButton->setEnabled(false);
    ui->ageSpinBox->setEnabled(false);
    ui->sizeComboBox->setEnabled(false);
    ui->sizeSpinBox->setEnabled(false);
}

MainWindow::~MainWindow() {
    delete ui;
}




void MainWindow::writeInfoList() {
    qDebug() << "Количетсво найденных файлов:" << plugin->getFilteredFiles().count();
   ui->stopButton->setEnabled(false);
   for(QFileInfo &fileInInfoFilelist : plugin->getFilteredFiles()) {
       ui->listWidget->addItem(fileInInfoFilelist.filePath());
   }
   ui->listWidget->insertItem(0,QString("[Найдено: файлов - " + QString::number(plugin->getFilteredFiles().count())) + "]");
   ui->stopButton->setEnabled(false);
   ui->startButton->setEnabled(true);
   ui->ageSpinBox->setReadOnly(false);
}

void MainWindow::stopButtonActivate(){
    ui->stopButton->setEnabled(true);
}


void MainWindow::on_choosePathButton_clicked() {
    path = QFileDialog::getExistingDirectory(this,tr("Open directory"),
                                             path.isEmpty() ? "/home" : path,
                                             QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(!path.isEmpty()) {
        ui->choosePathLineEdit->setText(path);
        ui->startButton->setEnabled(true);
    }
}

void MainWindow::testPlugins(QFileInfo pluginFileInfo) {
        qDebug() << "Loading: " << pluginFileInfo.filePath();

        QPluginLoader loader(pluginFileInfo.filePath());
        if(!loader.load())
            qDebug() << "Error:" << loader.fileName() << loader.errorString();
        this->plugin = qobject_cast<Plugin*>(loader.instance());
        if(this->plugin){
            qDebug() << "Loaded: " << loader.fileName();
            connect(plugin,SIGNAL(emitFile()),this,SLOT(writeInfoList()));
            connect(plugin, SIGNAL(emitCancel()), this, SLOT(stopButtonActivate()));
            ui->choosePathButton->setEnabled(true);
            qDebug() << "Done";
        }
        else{
            qDebug()<< "Error. Plugin doesn't load.";
        }
}

void MainWindow::on_choosePluginButton_clicked() {
    QString stringPath = QFileDialog::getOpenFileName(this,tr("Choose plugin"), "",tr("dll files (*.dll)"));
    if(!stringPath.isEmpty()){
        ui->choosePathButton->setEnabled(false);
        testPlugins(QFileInfo(stringPath));
    }
    else
        qDebug() << "Please, choose the correct dll file";

}

void MainWindow::on_startButton_clicked() {
    qDebug() << "started finding files";
    ui->startButton->setEnabled(false);
    ui->ageSpinBox->setReadOnly(true);
    ui->listWidget->clear();
    QStringList filters = QStringList() << ui->chooseComboBox->currentText();
    bool isAgeChecked = ui->ageCheckBox->isChecked();
    bool isSizeChecked = ui->sizeCheckBox->isChecked();
    int ageRequired = ui->ageSpinBox->text().toInt();
    int sizeRequired = ui->sizeSpinBox->text().toInt();
    bool moreLess;
    if(ui->sizeComboBox->currentText() == "<")
        moreLess = false;
    else
        moreLess = true;
    plugin->findFilteredFiles(this->path,filters,isAgeChecked,isSizeChecked,ageRequired,moreLess,sizeRequired);
}

void MainWindow::on_stopButton_clicked() {
    plugin->cancelFuture();
}

void MainWindow::on_ageCheckBox_stateChanged(int arg1)
{
    Q_UNUSED(arg1);
    if(!ui->ageCheckBox->isChecked())
        ui->ageSpinBox->setEnabled(false);
    else ui->ageSpinBox->setEnabled(true);
}

void MainWindow::on_sizeCheckBox_stateChanged(int arg1)
{
    Q_UNUSED(arg1);
    if(!ui->sizeCheckBox->isChecked()) {
        ui->sizeComboBox->setEnabled(false);
        ui->sizeSpinBox->setEnabled(false);
    }
    else {
        ui->sizeSpinBox->setEnabled(true);
        ui->sizeComboBox->setEnabled(true);
    }
}
