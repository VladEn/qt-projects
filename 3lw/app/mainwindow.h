#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QString>
#include <QPluginLoader>
#include <QDir>
#include <QFileInfoList>
#include <QFileInfo>
#include <QStringList>
#include <QFileDialog>

#include "../myplugin/plugin.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);  
    ~MainWindow();
    void testPlugins(QFileInfo plugin);
    QString path;
    Plugin* plugin;

public slots:
    void writeInfoList();
    void stopButtonActivate();

private slots:
    void on_choosePathButton_clicked();
    void on_choosePluginButton_clicked();
    void on_startButton_clicked();

    void on_stopButton_clicked();

    void on_ageCheckBox_stateChanged(int arg1);

    void on_sizeCheckBox_stateChanged(int arg1);

private:

    Ui::MainWindow *ui;

    QFileInfoList files;

};
#endif // MAINWINDOW_H
