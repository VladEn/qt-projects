#ifndef PLUGIN_H
#define PLUGIN_H


#include <QDebug>
#include <QString>
#include <QFileInfoList>
#include <QFuture>
#include <QFutureWatcher>


#include <QObject>
#include <QtPlugin>




class Plugin : public QObject
{

    Q_OBJECT

public:
    virtual ~Plugin(){};
    virtual void findFilteredFiles(const QString &path,QStringList &formatFilter, bool &isAgeChecked, bool &isSizeChecked,int &ageRequired, bool &moreLess,int &sizeRequired) = 0;
    virtual QFileInfoList getFilteredFiles() = 0;
    virtual void cancelFuture() = 0;
    virtual void cancelFutureWatcher() = 0;

// ===== SIGNALS ===== //
    virtual void emitFile() = 0;
    virtual void emitCancel() = 0;
};

Q_DECLARE_INTERFACE(Plugin, "ru.mephi")

#endif // PLUGIN_H
