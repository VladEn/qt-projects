#include "myplugin.h"

QString Myplugin::path;
QStringList Myplugin::formatFilter;
bool Myplugin::isAgeChecked;
bool Myplugin::isSizeChecked;
int Myplugin::ageRequired = 0;
int Myplugin::sizeRequired = 0;
bool Myplugin::moreLess = false;
QFileInfoList Myplugin::filteredFiles;
QFileInfoList Myplugin::formatFiles;
QMutex Myplugin::mutex;


Myplugin::Myplugin(QObject *parent) {
    Q_UNUSED(parent)
    connect(&watcherInfoList, &QFutureWatcher<QFileInfoList>::finished, this, &Myplugin::filterFiles);
    connect(&watcher, &QFutureWatcher<void>::finished, this, &Myplugin::emitQFileInfoList);
}

Myplugin::~Myplugin(){}


void Myplugin::findFilteredFiles(const QString &path, QStringList &formatFilter, bool &isAgeChecked, bool &isSizeChecked, int &ageRequired, bool &moreLess,int &sizeRequired) {
    Myplugin::formatFiles.clear();
    Myplugin::filteredFiles.clear();
    Myplugin::path = path;
    Myplugin::formatFilter = formatFilter;
    Myplugin::isAgeChecked = isAgeChecked;
    Myplugin::isSizeChecked = isSizeChecked;
    Myplugin::ageRequired = ageRequired;
    Myplugin::sizeRequired = sizeRequired;
    Myplugin::moreLess = moreLess;

    futureInfoList = QtConcurrent::run(findFormatFiles,path,formatFilter);
    watcherInfoList.setFuture(futureInfoList);
}

void Myplugin::filterFiles() {


    Myplugin::formatFiles = futureInfoList.result();
    if(!isAgeChecked && !isSizeChecked){
        future = QtConcurrent::filter(Myplugin::formatFiles, modifyNot);
        watcher.setFuture(future);
    }
    else if(isAgeChecked && !isSizeChecked){
        future = QtConcurrent::filter(Myplugin::formatFiles, modifyAge);
        watcher.setFuture(future);
    }
    else if(!isAgeChecked && isSizeChecked){
        future = QtConcurrent::filter(Myplugin::formatFiles, modifySize);
        watcher.setFuture(future);
    }
    else if(isAgeChecked && isSizeChecked){
        future = QtConcurrent::filter(Myplugin::formatFiles, modifySizeAndAge);
        watcher.setFuture(future);
    }

    emitCancelButton();
}

void Myplugin::cancelFuture() {
    this->watcher.future().cancel();
}

void Myplugin::cancelFutureWatcher() {
    this->watcher.cancel();
}



QFileInfoList Myplugin::findFormatFiles(const QString &path, const QStringList formatFilter) {
    QFileInfoList names;

    QDir dir(path);

    const auto files = dir.entryInfoList(formatFilter, QDir::Files);
    for(const auto &file : files)
        names += file.filePath();

    const auto subdirs = dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
    for(const auto &subdir : subdirs)
        names += findFormatFiles(path + '/' + subdir, formatFilter);

    return names;
}

bool Myplugin::modifyAge(const QFileInfo &file) {
    try {
        QDateTime time = file.birthTime();
        int ageOfFile = (int)time.daysTo(QDateTime::currentDateTime());
        if(ageOfFile < Myplugin::ageRequired){
            Myplugin::mutex.lock();
            Myplugin::filteredFiles.append(file);
            Myplugin::mutex.unlock();
            return true;
        }

    } catch (...) {
        qDebug()<< " omugud";
    }
    return false;
}

bool Myplugin::modifySize(const QFileInfo &file) {
    qint64 fileSize = file.size() / 1024;
    if(Myplugin::moreLess){
        if(fileSize > Myplugin::sizeRequired){
            Myplugin::mutex.lock();
            Myplugin::filteredFiles.append(file);
            Myplugin::mutex.unlock();
            return true;
        }
        else
            return false;
    }
    else{
        if(fileSize < Myplugin::sizeRequired) {
            Myplugin::mutex.lock();
            Myplugin::filteredFiles.append(file);
            Myplugin::mutex.unlock();
            return true;
        }
        else
            return false;
    }
}

bool Myplugin::modifySizeAndAge(const QFileInfo &file) {
    QDateTime time = file.birthTime();
    int ageOfFile = (int)time.daysTo(QDateTime::currentDateTime());
    double fileSize = ((double)file.size())/1024;
    if(ageOfFile < Myplugin::ageRequired){
        if(Myplugin::moreLess){
            if(fileSize > Myplugin::sizeRequired){
                Myplugin::mutex.lock();
                Myplugin::filteredFiles.append(file);
                Myplugin::mutex.unlock();
                return true;
            }
            else
                return false;
        }
        else{
            if(fileSize < Myplugin::sizeRequired){
                Myplugin::mutex.lock();
                Myplugin::filteredFiles.append(file);
                Myplugin::mutex.unlock();
                return true;
            }
            else
                return false;
        }
    }
    else
        return false;
}

bool Myplugin::modifyNot(const QFileInfo &file){
    Myplugin::mutex.lock();
    Myplugin::filteredFiles.append(file);
    Myplugin::mutex.unlock();
    return true;
}

void Myplugin::emitQFileInfoList() {
    emit emitFile();
}

void Myplugin::emitCancelButton() {
    emit emitCancel();
}

QFileInfoList Myplugin::getFilteredFiles() {
    return this->filteredFiles;
}
