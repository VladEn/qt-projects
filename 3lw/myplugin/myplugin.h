#ifndef MYPLUGIN_H
#define MYPLUGIN_H

#include <QObject>
#include <QtPlugin>
#include <QtDebug>
#include <QString>
#include <QDir>
#include <QFileInfoList>
#include <QFileInfo>
#include <QStringList>
#include <QDateTime>
#include <QFuture>
#include <QFutureWatcher>

#include <QtConcurrent/QtConcurrent>

#include "myplugin_global.h"
#include <plugin.h>

class MYPLUGIN_EXPORT Myplugin : public Plugin
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID "ru.mephi")
    Q_INTERFACES(Plugin)

signals:
    void emitFile() override;
    void emitCancel() override;


public:
    explicit Myplugin(QObject *parent = nullptr);
    ~Myplugin();
    void findFilteredFiles(const QString &path, QStringList &formatFilter, bool &isAgeChecked, bool &isSizeChecked, int &ageRequired, bool &moreLess,int &sizeRequired) override;
    static QFileInfoList findFormatFiles(const QString &pathdir , const QStringList filters);

    QFuture<void> future;
    QFutureWatcher<void> watcher;

    QFuture<QFileInfoList> futureInfoList;
    QFutureWatcher<QFileInfoList> watcherInfoList;


    static QString path;
    static QStringList formatFilter;
    static bool isAgeChecked;
    static bool isSizeChecked;
    static int ageRequired;
    static int sizeRequired;
    static bool moreLess;
    static QFileInfoList filteredFiles;
    static QFileInfoList formatFiles;


    // Plugin interface
public:
    static bool modifyAge(const QFileInfo &file);
    static bool modifySize(const QFileInfo &file);
    static bool modifySizeAndAge(const QFileInfo &file);
    static bool modifyNot(const QFileInfo &file);
    void emitQFileInfoList();
    void emitCancelButton();
    QFileInfoList getFilteredFiles() override;
    void filterFiles();
    void cancelFuture() override;
    void cancelFutureWatcher() override;

    static QMutex mutex;



};

//int Myplugin::ageRequired = 0;
//int Myplugin::sizeRequired = 0;
//bool Myplugin::moreLess = false;

#endif // MYPLUGIN_H
