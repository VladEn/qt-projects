#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QProcess>
#include <QDebug>
#include <QTextCodec>
#include <QTime>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

public slots:
    void finish();
    void start();
    void standardError();
    void standardOutput();

private:
    Ui::MainWindow *ui;

    QString filename="";
    QProcess *child;
    QStringList arguments;
    QTime time;
};
#endif // MAINWINDOW_H
