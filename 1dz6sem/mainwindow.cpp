#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->pushButton_2->setDisabled(true);
    ui->lineEdit->setReadOnly(true);
    ui->exitCode->setReadOnly(true);
    ui->pushButton->setDisabled(true);
    ui->plainTextEdit->setReadOnly(true);
    child = new QProcess(this);
    connect(child,SIGNAL(finished(int)),this,SLOT(finish()));
    connect(child,SIGNAL(started()),this,SLOT(start()));
    connect(child,SIGNAL(readyReadStandardError()),this,SLOT(standardError()));
    connect(child,SIGNAL(readyReadStandardOutput()),this,SLOT(standardOutput()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::standardError()
{
    //QByteArray array = child->readAllStandardError();
    //QString DataAsString = QTextCodec::codecForMib(1015)->toUnicode(array);
    QTextCharFormat fmt = ui->plainTextEdit->currentCharFormat();
    fmt.setForeground(QBrush(Qt::red));
    ui->plainTextEdit->setCurrentCharFormat(fmt);
    QString DataAsString = QString::fromUtf8(child->readAllStandardError().data());
    this->ui->plainTextEdit->appendPlainText(DataAsString);
    fmt = ui->plainTextEdit->currentCharFormat();
    fmt.clearForeground();
    ui->plainTextEdit->setCurrentCharFormat(fmt);


}

void MainWindow::standardOutput()
{
    //QByteArray array = child->readAllStandardOutput();
    //QString DataAsString = QTextCodec::codecForMib(1015)->toUnicode(array);
    QTextCharFormat fmt = ui->plainTextEdit->currentCharFormat();
    fmt.setForeground(QBrush(Qt::blue));
    ui->plainTextEdit->setCurrentCharFormat(fmt);
    QString DataAsString = QString::fromUtf8(child->readAllStandardOutput().data());
    this->ui->plainTextEdit->appendPlainText(DataAsString);
    fmt = ui->plainTextEdit->currentCharFormat();
    fmt.clearForeground();
    ui->plainTextEdit->setCurrentCharFormat(fmt);
}

void MainWindow::start()
{
    this->ui->plainTextEdit->clear();
    qDebug()<<filename << " started";
    QTime currentTime = QTime::currentTime();
    this->ui->plainTextEdit->appendPlainText("["+currentTime.toString("hh:mm:ss")+"]");
    this->ui->plainTextEdit->insertPlainText(filename+" started ");
    this->ui->pushButton_2->setEnabled(true);
}

void MainWindow::finish()
{
    QTime currentTime = QTime::currentTime();
    this->ui->plainTextEdit->appendPlainText("["+currentTime.toString("hh:mm:ss")+"]");
    this->ui->plainTextEdit->insertPlainText(filename+" finished ");
    qDebug()<<time.elapsed();
    int mseconds = time.elapsed();
    int seconds = time.elapsed()/1000;
    int minutes = seconds / 60;
    int hours = minutes /60;
    minutes = minutes %60;
    seconds = seconds %60;
    mseconds = mseconds %1000;
    qDebug() << hours << " " << minutes << " " << seconds << " " << mseconds;
    this->ui->plainTextEdit->appendPlainText("duration: " +QString::number(hours)+":"+QString::number(minutes)+":"+QString::number(seconds)+":"+QString::number(mseconds));
    qDebug()<<filename << " finished";
    QString string = QString::number(child->exitCode());
    ui->exitCode->setText(string);
    ui->pushButton->setEnabled(true);
    ui->pushButton_2->setDisabled(true);
}


void MainWindow::on_pushButton_3_clicked()
{
    this->filename = QFileDialog::getOpenFileName(this,"", "",tr("exe files (*.exe)"));
    this->ui->lineEdit->setText(filename);
    this->ui->pushButton->setEnabled(true);
}

void MainWindow::on_pushButton_clicked()
{
    arguments = QStringList(ui->lineEdit_2->text().split(" "));
    qDebug() << arguments;
    this->child->start(filename,arguments);
    time.start();
    this->ui->pushButton_2->setEnabled(true);
    this->ui->pushButton->setDisabled(true);
    if(!child->waitForStarted())
        qDebug()<<"false";
}

void MainWindow::on_pushButton_2_clicked()
{
    child->terminate();
    qDebug() << "terminate";
}
